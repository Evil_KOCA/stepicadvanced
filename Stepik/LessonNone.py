# Для того, чтобы проверить значение переменной на None, мы используем либо оператор is, либо оператор проверки на
# равенство ==.
var = None
print(type(var))
var = None
if var is None:   # используем оператор is
  print('None')
else:
  print('Not None')

var = None
if var == None:  # используем оператор ==
  print('None')
else:
  print('Not None')
print(None == None)
print(None == 17)
print(None == 3.14)
print(None == True)
print(None == [1, 2, 3])
print(None == 'Beegeek')
print(None == 0)
print(None == False)
print(None == '')
# print(None > 0) TypeError: '>' not supported between instances of 'NoneType' and 'int' ('bool')
# print(None <= False) TypeError: '>' not supported between instances of 'NoneType' and 'int' ('bool')

# Обратите внимание, что функции, не возвращающие значений, на самом деле в Python возвращают значение None.
def print_message():
  print('Я - Тимур,')
  print('король матана. ')


list1 = ['a', 'b', 'c', 'd']
list2 = ['e', 'f', 'g']
list3 = list1 + list2
print(list3)
list1.extend('35')
print(list1)

zeros = [0] * 10
print(len(zeros))

numbers = [10, 20, 30, 40, 50]
print(numbers[-2])
print(numbers[-4:-1])

numbers = [10, 20, 30, 40, 50, 60, 70, 80]
print(numbers[2:5])
print(numbers[:4])
print(numbers[3:])

numbers = [4, 8, 12, 16, 34, 56, 100]
numbers[1:4] = [20, 24, 28]
print(numbers)

numbers = [5, 10, 15, 25]
print(numbers[::-2])

numbers = [10, 20, 30, 40, 50]
numbers.append(60)
print(numbers)
numbers.append(60)
print(numbers)
print()

numbers = [10, 20, 30, 40, 50]
a = numbers.pop()
print(a)
print(numbers)

numbers.pop(2)
print(numbers)

letters = ['a', 'b', 'c', 'd']

new_letters = list(letters)
print(new_letters)

new_letters = letters.copy()
print(new_letters)

new_letters = letters[:]
print(new_letters)

words = ['Hello', 'Python']
print('-'.join(words))

words = ['xyz', 'zara', 'beegeek']
print(max(words))
list1 = [10, 20, [300, 400, [5000, 6000], 500], 30, 40]
a = [10, 20, [300, 400, [5000, 6000, 7000], 500], 30, 40]
list1[2][2].append(7000)
print(list1)


list1 = ['a', 'b', ['c', ['d', 'e', ['f', 'g'], 'k'], 'l'], 'm', 'n']
sub_list = ['h', 'i', 'j']
list1[2][1][2].extend(sub_list)
print(list1)

list1 = [[1, 7, 8], [9, 7, 102], [6, 106, 105], [100, 99, 98, 103], [1, 2, 3]]
maximum = -1
for i in list1:
    if max(i) > maximum:
      maximum = max(i)
print(maximum)


list1 = [[1, 7, 8], [9, 7, 102], [102, 106, 105], [100, 99, 98, 103], [1, 2, 3]]
for i in range(len(list1)):
  list1[i] = list1[i][::-1]

print(list1)

list1 = [[1, 7, 8], [9, 7, 102], [102, 106, 105], [100, 99, 98, 103], [1, 2, 3]]
total = 0
counter = 0
for i in list1:
  for j in i:
    total += j
    counter += 1
print(total/counter)

list1 = [[1] * 3] * 3
print(list1)
list1[0][1] = 5
print(list1)
print(list1[1][1])