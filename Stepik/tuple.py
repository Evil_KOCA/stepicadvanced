
my_list = [1, 2, 3, 4, 5]
my_list[0] = 9
my_list[4] = 7
print(my_list)
# Кортеж (tuple) – ещё один вид коллекций в Python. Похож на список, но, в отличие от списка, неизменяемый.
my_tuple = (1, 2, 3, 4, 5)
#  my_tuple[0] = 9 приводит к ошибке
#  my_tuple[4] = 7 приводит к ошибке
print(my_tuple)

# Примеры кортежей
empty_tuple = ()                                      # пустой кортеж
point = (1.5, 6.0)                                    # кортеж из двух чисел
names = ('Timur', 'Ruslan', 'Roman')                  # кортеж из трех строк
info = ('Timur', 'Guev', 28, 170, 60, False)          # кортеж из 6 элементов разных типов
nested_tuple = (('one', 'two'), ['three', 'four'])    # кортеж из кортежа и списка
# в переменной empty_tuple хранится пустой кортеж; в переменной point хранится кортеж, состоящий из двух вещественных
# чисел (такой кортеж удобно использовать для представления точки на координатной плоскости); в переменной names
# хранится кортеж, содержащий три строковых значения; в переменной info содержится кортеж, содержащий 66 элементов
# разного типа (строки, числа, булевы переменные); в переменной nested_tuple содержится кортеж, содержащий другой
# кортеж и список.

# Для создания кортежа с единственным элементом после значения элемента ставят замыкающую запятую:
my_tuple = (1,)
print(type(my_tuple))     # <class 'tuple'>

# Если запятую пропустить, то кортеж создан не будет. Например, приведенный ниже код просто присваивает переменной
# my_tuple целочисленное значение 1:
my_tuple = (1)
print(type(my_tuple))     # <class 'int'>


def get_powers(num):
 return num ** 2, num ** 3, num ** 4


result = get_powers(5)
print(type(result))
print(result)


my_tuple = (1, 'python', [1, 2, 3])
print(my_tuple)
my_tuple[2][0] = 100
my_tuple[2].append(17)
print(my_tuple)

# При этом важно понимать: меняется список, а не кортеж. Списки являются ссылочными типами данных, поэтому в кортеже
# хранится ссылка на список, которая не меняется при изменении самого списка.


# Встроенная функция list() может применяться для преобразования кортежа в список.
number_tuple = (1, 2, 3, 4, 5)
number_list = list(number_tuple)
print(number_list)

# Встроенная функция tuple()  может применяться для преобразования списка в кортеж.
str_list = ['один', 'два', 'три']
str_tuple = tuple(str_list)
print(str_tuple)


# Аналогичным образом мы можем создать кортеж на основании строки.
text = 'hello python'
str_tuple = tuple(text)
print(str_tuple)

# Преобразование строки в список позволяет получить список символов строки. Это может быть полезно, например,
# когда надо изменить один символ строки:
s = 'симпотичный'
print(s)
a = list(s)
a[4] = 'а'
s = ''.join(a)
print(s)

# С этой же целью может потребоваться преобразование кортежа в список:
writer = ('Лев Толстой', 1827)
print(writer)
a = list(writer)
a[1] = 1828
writer = tuple(a)
print(writer)

#  Операция конкатенации + и умножения на число *
print((1, 2, 3, 4) + (5, 6, 7, 8))
print((7, 8) * 3)
print((0,) * 10)


a = (1, 2, 3, 4)
b = (7, 8)
a += b   # добавляем к кортежу a кортеж b
b *= 5   # повторяем кортеж b 5 раз

print(a)
print(b)

numbers = (1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
print('Сумма всех элементов кортежа =', sum(numbers))
numbers = (3, 4, 10, 3333, 12, -7, -5, 4)
print('Минимальный элемент кортежа =', min(numbers))
print('Максимальный элемент кортежа =', max(numbers))

names = ('Gvido', 'Roman' , 'Timur')
position = names.index('Timur')
print(position)

# Чтобы избежать таких ошибок, можно использовать метод index() вместе с оператором принадлежности in:
names = ('Gvido', 'Roman' , 'Timur')
if 'Anders' in names:
    position = names.index('Anders')
    print(position)
else:
    print('Такого значения нет в кортеже')


names = ('Timur', 'Gvido', 'Roman', 'Timur', 'Anders', 'Timur')
cnt1 = names.count('Timur')
cnt2 = names.count('Gvido')
cnt3 = names.count('Josef')

print(cnt1)
print(cnt2)
print(cnt3)


colors = ('red', ('green', 'blue'), 'yellow')
numbers = (1, 2, (4, (6, 7, 8, 9)), 10, 11)
print(colors[1][1])
print(numbers[2][1][3])

countries = ('Russia', 'Argentina', 'Slovakia', 'Canada', 'Slovenia', 'Italy', 'Spain', 'Ukraine', 'Chile', 'Cameroon')
print(countries[-len(countries):-4])


countries = ('Russia', 'Argentina', 'Spain', 'Slovakia', 'Canada', 'Slovenia', 'Italy')
index = countries.index('Slovenia')
print(index)

countries = ('Russia', 'Argentina', 'Spain', 'Slovakia', 'Canada', 'Slovenia', 'Italy', 'Spain', 'Ukraine', 'Chile', 'Spain', 'Cameroon')
number = countries.count('Spain')
print(number)

numbers1 = (1, 2, 3)
numbers2 = (6,)
numbers3 = (7, 8, 9, 10, 11, 12, 13)
a = numbers1 * 2 + numbers2 * 9 + numbers3
print(a)


tuples = [(), (), ('',), ('a', 'b'), (), ('a', 'b', 'c'), (1,), (), (), ('d',), ('', ''), ()]
non_empty_tuples = [i for i in tuples if len(i) != 0]

print(non_empty_tuples)


tuples = [(10, 20, 40), (40, 50, 60), (70, 80, 90), (10, 90), (1, 2, 3, 4), (5, 6, 10, 2, 1, 77)]
sto = (100, )
new_tuples = [i[-len(i):-1:1] + sto for i in tuples]
print(new_tuples)
