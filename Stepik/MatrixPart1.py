# Вывести матрицу 1
# Вывести матрицу 2
# n = int(input())
# m = int(input())
# a = [[input() for j in range(m)] for i in range(n)]
#
# for i in a:
#     print(*i)
# print()
# for i in range(m):
#     for j in range(n):
#         print(a[j][i], end=' ')
#     print()


# След матрицы

# n = int(input())
# matrix = []
# for i in range(n):
#     temp = [int(num) for num in input().split()]
#     matrix.append(temp)
#
# k = 0
# for i in range(n):
#     k += matrix[i][i]
# print(k)

# Больше среднего
# n = int(input())
# matrix = []
# for i in range(n):
#     temp = [int(num) for num in input().split()]
#     matrix.append(temp)
# sr = []
#
# for i in matrix:
#     k = 0
#     for j in i:
#         k += j
#     sr += [k / len(i)]
#
# for i in range(n):
#     k = 0
#     for j in range(n):
#         if matrix[i][j] > sr[i]:
#             k += 1
#     print(k)

# Максимальный в области 1
# n = int(input())
# m = []
# for i in range(n):
#     temp = [int(num) for num in input().split()]
#     m.append(temp)
# sr = m[0][0]
# for i in range(n):
#     for j in range(i + 1):
#         if m[i][j] > sr:
#             sr = m[i][j]
# print(sr)


# Максимальный в области 2 🌶️
# n = int(input())
# m = []
# for i in range(n):
#     temp = [int(num) for num in input().split()]
#     m.append(temp)
# sr = []
# for i in range(n):
#     for j in range(n):
#         if j <= i <= n - 1 - j or j >= i >= n - 1 - j:
#             sr += [m[i][j]]
# print(max(sr))


# Суммы четвертей
# n = int(input())
# m = []
# for i in range(n):
#     temp = [int(num) for num in input().split()]
#     m.append(temp)
# sr = {'Верхняя четверть:': 0, 'Правая четверть:': 0, 'Нижняя четверть:': 0, 'Левая четверть:': 0}
# for i in range(n):
#     for j in range(n):
#         if i < n - 1 - j and i < j:
#             sr['Верхняя четверть:'] += m[i][j]
#         elif i < n - 1 - j and i > j:
#             sr['Левая четверть:'] += m[i][j]
#         elif i > n - 1 - j and i < j:
#             sr['Правая четверть:'] += m[i][j]
#         elif i > n - 1 - j and i > j:
#             sr['Нижняя четверть:'] += m[i][j]
# for i, j in sr.items():
#     print(i, j)

# Таблица умножения
# m = int(input())
# n = int(input())
#
#
# l = [[0 for i in range(m)] for j in range(n)]
# for i in range(n):
#     for j in range(m):
#         l[i][j] = i * j
# for i in range(m):
#     for j in range(n):
#         print(str(l[j][i]).ljust(3), end=' ')
#     print()

# Максимум в таблице
# n = int(input())
# l = int(input())
# m = []
# for i in range(n):
#     temp = [int(num) for num in input().split()]
#     m.append(temp)
# win = m[0][0]
# for i in m:
#     for j in i:
#         if j > win:
#             win = j
# q = []
# for i in range(n):
#     for j in range(l):
#         if m[i][j] == win:
#             q.append([i, j])
# a = q[0]
# for i in q:
#     if i < a:
#         a = i
# print(*a)


# Обмен столбцов
# import mlib # Вынес функции в либу mlib
#
# n = int(input())
# l = int(input())
# m = []
# for i in range(n):
#     temp = [int(num) for num in input().split()]
#     m.append(temp)
# # temp = [int(num) for num in input().split()]
#
#
# a = mlib.tr(m)
# a = mlib.swapkol(temp[0], temp[1], a)
# lo = mlib.tr(a)
# mlib.printmat(lo)

# Симметричная матрица
# n = int(input())
# m = []
# for i in range(n):
#     temp = [int(num) for num in input().split()]
#     m.append(temp)
#
#
# s = []
# for i in range(1, n):
#     for j in range(n):
#         if m[i][j] == m[j][i]:
#             s += [1]
#         elif m[i][j] != m[j][i]:
#             s += [0]
#
# if s.count(0) > 0:
#     print('NO')
# else:
#     print('YES')


# Обмен диагоналей
# import mlib
# n = int(input())
# m = []
# for i in range(n):
#     temp = [int(num) for num in input().split()]
#     m.append(temp)
#
#
# qp = []
# qs = []
#
# for i in range(n):
#     qp += [m[i][i]]
#     qs += [m[n - 1 - i][i]]
#
# for i in range(n):
#     m[i][i] = qs[i]
#     m[n - 1 - i][i] = qp[i]
# # mlib.printmat(m)
#
# for i in m:
#     for j in i:
#         print(j, end=' ')
#     print()

# Зеркальное отображение
# n = int(input())
# m = []
# for i in range(n):
#     temp = [int(num) for num in input().split()]
#     m.append(temp)
#
# for i in m[::-1]:
#     for j in i:
#         print(j, end=' ')
#     print()

# Поворот матрицы
# n = int(input())
# m = []
# for i in range(n):
#     temp = [int(num) for num in input().split()]
#     m.append(temp)
#
#
# def tr(po):
#     ni = len(po)
#     nj = len(po[0])
#     z = [[0 for i in range(ni)] for j in range(nj)]
#     for ii in range(ni):
#         for j in range(nj):
#             z[j][ii] = po[ii][j]
#     return z
#
#
# a = tr(m)
# for i in a:
#     print(*i[::-1])

# Ходы коня
# n = 8 # ПОЛЕ
# m = [['.' for i in range(n)] for j in range(n)]
# a = [i for i in range(8)]
# b = [i for i in 'abcdefgh']
# d = {key: value for key, value in zip(b, a)}
# l = [[1, -2], [1, +2], [-1, -2], [-1, +2], [2, -1], [2, 1], [-2, -1], [-2, 1]]
#
# a = input() # Ввод поля, где стоит конь
# y = d[a[0]]
# x = n - int(a[1])
#
# m[x][y] = 'N'
# for i in range(n):
#     for j in range(n):
#         if m[i][j] == 'N':
#             for b in l:
#                 f = i + b[0]
#                 g = j + b[1]
#                 if 0 <= f <= n - 1 and 0 <= g <= n - 1:
#                     m[f][g] = '*'
# for i in m:
#     print(*i)


# Магический квадрат 🌶️
#
# def tr(po):
#     ni = len(po)
#     nj = len(po[0])
#     z = [[0 for i in range(ni)] for j in range(nj)]
#     for ii in range(ni):
#         for j in range(nj):
#             z[j][ii] = po[ii][j]
#     return z
#
#
# n = int(input())
# m = []
# for i in range(n):
#     temp = [int(num) for num in input().split()]
#     m.append(temp)
#
# ae = [] # Все элементы по порядку
# for i in m:
#     for j in i:
#         ae += [j]
# k = sorted(ae)
#
# qp = []
# qs = []
#
# for i in range(n): # Диагонали
#     qp.append(m[i][i])
#     qs.append(m[i][n - 1 - i])
#
# mtr = tr(m)
# # print('Главная Диагональ', qp)
# # print('Побочная Диагональ', qs)
# # print('Столбцы', mtr)
# # print(sum(k), sum(k) / n)
# et = [i + 1 for i in range(n * n)]
# tre = True and k == et
# for i in range(n):
#     tre = tre & (sum(m[i]) == sum(mtr[i]) == sum(qp) == sum(qs) == (sum(ae) / n) == (int(sum(et) / n)))
# if tre == True:
#     print('YES')
# elif tre == False:
#     print('NO')
#


# Шахматная доска
# a = [int(num) for num in input().split()]
#
# n = a[0]
# k = a[1]
#
# m = [[0 for i in range(k)] for j in range(n)]
#
#
# for i in range(n):
#     for j in range(k):
#         if (i + j) % 2 == 0:
#             m[i][j] = '.'
#         elif (i + j) % 2 == 1:
#             m[i][j] = '*'
# for i in m:
#     print(*i)


# Побочная диагональ
# n = int(input())
# m = [[0 for i in range(n)] for j in range(n)]
# for i in range(n):
#     for j in range(n):
#         if i < n - 1 - j:
#             m[i][j] = 0
#         elif i == n - 1 - j:
#             m[i][j] = 1
#         else:
#             m[i][j] = 2
#
# for i in m:
#     print(*i)


# Заполнение 1

# a = [int(num) for num in input().split()]
#
# n = a[0]
# k = a[1]
#
# l = 1
# m = [[0 for i in range(k)] for j in range(n)]
# for i in range(n):
#     for j in range(k):
#         m[i][j] = l
#         l += 1
#
# # for i in range(n):
# #     for j in range(k):
# #         m[i][j] = str(m[i][j])
#
# for i in m:
#     for j in i:
#         print(str(j).ljust(3), end='')
#     print()


# Заполнение 2
#
# def tr(po):
#     ni = len(po)
#     nj = len(po[0])
#     z = [[0 for i in range(ni)] for j in range(nj)]
#     for ii in range(ni):
#         for j in range(nj):
#             z[j][ii] = po[ii][j]
#     return z
#
#
# a = [int(num) for num in input().split()]
#
# k = a[0]
# n = a[1]
#
# l = 1
# m = [[0 for i in range(k)] for j in range(n)]
# for i in range(n):
#     for j in range(k):
#         m[i][j] = l
#         l += 1
#
# m = tr(m)
# for i in m:
#     for j in i:
#         print(str(j).ljust(3), end='')
#     print()


# Заполнение 3

# n = int(input())
# m = [[0 for i in range(n)] for j in range(n)]
# for i in range(n):
#     for j in range(n):
#         if i == j or i == n - 1 - j:
#             m[i][j] = 1
#         else:
#             m[i][j] = 0
#
# for i in m:
#     print(*i)

# Заполнение 4
# sr = {'Верхняя четверть:': 0, 'Правая четверть:': 0, 'Нижняя четверть:': 0, 'Левая четверть:': 0}


# n = int(input())
# m = [[0 for i in range(n)] for j in range(n)]
# for i in range(n):
#     for j in range(n):
#         if i >= n - 1 - j and i >= j or i <= n - 1 - j and i <= j:
#             m[i][j] = 1
#         else:
#             m[i][j] = 0
#
# for i in m:
#     print(*i)


# Заполнение 5 🌶️

# a = [int(num) for num in input().split()]
#
# k = a[0]
# n = a[1]
#
# l = [i + 1 for i in range(n)] + [i + 1 for i in range(n)]
#
#
# m = []
# for i in range(k):
#     temp = l[i % n : i % n + n]
#     m.append(temp)
# for i in m:
#     print(*i)

# Заполнение змейкой
# a = [int(num) for num in input().split()]
#
# n = a[0]
# k = a[1]
#
# l = 1
# m = [[0 for i in range(k)] for j in range(n)]
# for i in range(n):
#     for j in range(k):
#         m[i][j] = l
#         l += 1
# l = 0
# for i in range(1, n, 2):
#     m[i] = m[i][::-1]
#
# for i in m:
#     for j in i:
#         print(str(j).ljust(3), end=' ')
#     print()


# Заполнение диагоналями 🌶️
# a = [int(num) for num in input().split()]
#
# n = a[0]
# k = a[1]
#
# l = 1
# m = [[0 for i in range(k)] for j in range(n)]
#
# for z in range(n + k):
#     for i in range(n):
#         for j in range(k):
#             if i + j == z:
#                 m[i][j] = l
#                 l += 1
#
#
# for i in m:
#     for j in i:
#         print(str(j).ljust(4), end=' ')
#     print()


# Заполнение спиралью 😈😈

# ввод размеров матрицы
# s = [int(num) for num in input().split()]
#
# n = s[0]
# k = s[1]
#
# a = [[0 for i in range(k)] for j in range(n)]
#
#
# l = 1
# for i in range(n):
#     for j in range(k):  # НА ВОСТОК
#         if a[i][j] == 0:
#             a[i][j] = l
#             l += 1
#
#     for j in range(n):  # НА ЮГ
#         if a[j][(k - 1 - i) % k] == 0:
#             a[j][k - 1 - i] = l
#             l += 1
#
#     for j in range(k):  # НА ЗАПАД
#         if a[n - 1 - i][k - 1 - j] == 0:
#             a[n - 1 - i][k - 1 - j] = l
#             l += 1
#     for j in range(n):  # НА СЕВЕР (ТАБАКИ ОДОБРЯЕТ)
#         if i > n:
#             break
#         if a[n - 1 - j][i % k] == 0:
#             a[n - 1 - j][i % k] = l
#             l += 1
#
#
# print()
# for i in a:
#     for j in i:
#         print(str(j).ljust(3), end=" ")
#     print()


# import mlib
#
# a = [[k for i in range(1, 4)] for k in range(1, 4)]
# b = [[0 for i in range(1, 4)] for k in range(1, 4)]
# l = 1
# for i in range(3):
#     for j in range(3):
#         b[i][j] = l
#         l += 1
#
# mlib.printmat(a)
# mlib.printmat(b)
#
# mlib.printmat(mlib.summ(a,b))


# Сложение матриц


# def summ(po1, po2):
#     po1x = len(po1)
#     po1y = len(po1[0])
#     po2x = len(po2)
#     po2y = len(po2[0])
#     if po1x == po2x or po1y == po2y:  # число строк первой матрицы равно числу столбцов второй
#         po3 = [[0 for i in range(po1y)] for j in range(po1x)]
#         for i in range(po1x):
#             for j in range(po1y):
#                 po3[i][j] = po1[i][j] + po2[i][j]
#         return po3
#     elif po1x != po2x or po1y != po2y:
#         print('Матрицы НЕЛЬЗЯ СЛОЖИТЬ')
#
#
# s = [int(num) for num in input().split()]
# n = s[0]
# k = s[1]
#
# a = []
# b = []
#
# for i in range(n):
#     temp = [int(num) for num in input().split()]
#     a.append(temp)
# input()
# for i in range(n):
#     temp = [int(num) for num in input().split()]
#     b.append(temp)
#
#
#
# c = summ(a, b)
#
# for i in c:
#     for j in i:
#         print(j, end=' ')
#     print()


# Умножение матриц 🌶️

# def multi(po1, po2):
#     po1x = len(po1)
#     po1y = len(po1[0])
#     po2x = len(po2)
#     po2y = len(po2[0])
#     if po1y == po2x:  # число строк первой матрицы равно числу столбцов второй
#         po3 = [[0 for i in range(po2y)] for j in range(po1x)]
#         for i in range(po2y):
#             for j in range(po1x):
#                 for k in range(po1y):
#                     po3[i][j] += po1[i][k] * po2[k][j]
#         return po3
#     elif po1y != po2x:
#         print('Матрицы НЕЛЬЗЯ перемножить')
#
#
# s = [int(num) for num in input().split()]
# n = s[0]
# a = []
#
# for i in range(n):
#     temp = [int(num) for num in input().split()]
#     a.append(temp)
# input()
#
# s = [int(num) for num in input().split()]
# n = s[0]
# b = []
# for i in range(n):
#     temp = [int(num) for num in input().split()]
#     b.append(temp)
#
# c = multi(a, b)
# for i in c:
#     for j in i:
#         print(j,end=' ')
#     print()

# Возведение матрицы в степень 🌶️


def multi(po1, po2):
    po1x = len(po1)
    po1y = len(po1[0])
    po2x = len(po2)
    po2y = len(po2[0])
    if po1y == po2x:  # число строк первой матрицы равно числу столбцов второй
        po3 = [[0 for i in range(po2y)] for j in range(po1x)]
        for i in range(po2y):
            for j in range(po1x):
                for k in range(po1y):
                    po3[i][j] += po1[i][k] * po2[k][j]
        return po3
    elif po1y != po2x:
        print('Матрицы НЕЛЬЗЯ перемножить')


def power(k, po):
    po1 = [[0 for i in range(len(po[0]))] for j in range(len(po))]
    for i in range(len(po)):
        for j in range(len(po[0])):
            po1[i][j] = po[i][j]
    for i in range(1, k):
        po1 = multi(po1, po)
    return po1


n = int(input())
a = []

for i in range(n):
    temp = [int(num) for num in input().split()]
    a.append(temp)
p = int(input())

c = power(p, a)
for i in c:
    for j in i:
        print(j, end=' ')
    print()
