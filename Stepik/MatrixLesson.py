matrix = [[2, -5, -11, 0],
           [-9, 4, 6, 13],
           [4, 7, 12, -2]]


print(matrix[1][2])  # вывод элемента на позиции (2, 3)
print()

# Чтобы перебрать элементы матрицы, необходимо использовать вложенные циклы. Например, выведем на экран все элементы
# матрицы, перебирая их по строкам:

rows, cols = 3, 4           # rows - количество строк, cols - количество столбцов

matrix = [[2, 3, 1, 0],
           [9, 4, 6, 8],
           [4, 7, 2, 7]]

for r in range(rows):
    for c in range(cols):
        print(matrix[r][c], end=' ')
    print()


# Для перебора элементов матрицы по столбцам можно использовать следующий код:

rows, cols = 3, 4           # rows - количество строк, cols - количество столбцов

matrix = [[2, 3, 1, 0],
        [9, 4, 6, 8],
        [4, 7, 2, 7]]

print()
for c in range(cols):
    for r in range(rows):
        print(matrix[r][c], end=' ')
    print()


# Метод ljust()
print()
print('Метод ljust()')
print('a'.ljust(3))
print('ab'.ljust(3))
print('abc'.ljust(3))
print('abcdefg'.ljust(3))
print('a'.ljust(5, '*'))
print('ab'.ljust(5, '$'))
print('abc'.ljust(5, '#'))

# Метод rjust()
print()
print('Метод rjust()')
print('a'.rjust(3))
print('ab'.rjust(3))
print('abc'.rjust(3))
print('abcdefg'.rjust(3))
print('a'.rjust(5, '*'))
print('ab'.rjust(5, '$'))
print('abc'.rjust(5, '#'))

# Применив метод ljust() для выравнивания столбцов при выводе таблицы мы получим следующий код:
rows, cols = 3, 4                # rows - количество строк, cols - количество столбцов

matrix  = [[277, -930, 11, 0],
           [9, 43, 6, 87],
           [4456, 8, 290, 7]]

for r in range(rows):
    for c in range(cols):
        print(str(matrix[r][c]).ljust(6), end='')
    print()



print()
# Элементы с индексами i и j, связанными соотношением i + j + 1 = n (или j = n - i - 1), где n — размерность матрицы,
# находятся на побочной диагонали.
print('диагонали')

n = 8
matrix = [[0]*n for _ in range(n)]    # создаем квадратную матрицу размером 8×8

for i in range(n):                     # заполняем главную диагональ 1-цами, а побочную 2-ками
    matrix[i][i] = 1
    matrix[i][n-i-1] = 2

for r in range(n):                     # выводим матрицу
    for c in range(n):
        print(matrix[r][c], end=' ')
    print()


# Используйте функцию print_matrix() для вывода квадратной матрицы размерности n:
def print_matrix(matrix, n, width=1):
    for r in range(n):
        for c in range(n):
            print(str(matrix[r][c]).ljust(width), end=' ')
        print()

# Для считывания квадратной матрицы размерности n, заполненной числами, удобно использовать следующий код:
# n = int(input())
# matrix = []
# for i in range(n):
#     temp = [int(num) for num in input().split()]
#     matrix.append(temp)


n = 5
a = [[19, 21, 33, 78, 99],
     [41, 53, 66, 98, 76],
     [79, 80, 90, 60, 20],
     [33, 11, 45, 67, 90],
     [45, 67, 12, 98, 23]]

maximum = -1
minimum = 100

for i in range(n):
    if a[i][i] > maximum:
        maximum = a[i][i]
    if a[i][n - i - 1] < minimum:
        minimum = a[i][n - i - 1]
print(minimum + maximum)