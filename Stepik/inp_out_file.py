# a = input()
#
# with open(a, 'r') as inf:
#     s = inf.read()
#     print(s)

# a = 'lines.txt'
# with open(a, 'r') as inf:
#     s = inf.readlines()
#     print(s[-2].rstrip())


# import random
#
# with open(r'C:\Users\n.kan\lines.txt', 'r', encoding='utf-8') as inf:
#     a = inf.readlines()
#     t = random.randint(0, len(a)-1)
#     print(a[t].rstrip())


# with open(r'C:\Users\n.kan\numbers.txt', 'r', encoding='utf-8') as inf:
#     s = sum(map(lambda x: int(x.rstrip()), inf.readlines()))
#     print(s)
#     inf.close()


# Сумма двух-2
# with open(r'C:\Users\n.kan\nums.txt', 'r', encoding='utf-8') as inf:
#     print(sum(map(int, (filter(lambda x: x != '', map(lambda a: a.rstrip(), inf.readlines()))))))
#     inf.close()


# Общая стоимость

# with open(r'C:\Users\n.kan\prices.txt', 'r', encoding='utf-8') as inf:
#     q = map(lambda x: x.rstrip().split(), inf.readlines())
#     k = 0
#     for i in q:
#         a,b,c = i
#         k += int(b) * int(c)
#     print(k)

# Обратный порядок
# with open(r'C:\Users\n.kan\text.txt', encoding='utf-8') as file:
#     a = file.readline().rstrip()
#     print(a[::-1])


# Длинные строки
# with open(r'C:\Users\n.kan\lines.txt', encoding='utf-8') as file:
#     a = list(map(lambda x: x.rstrip(), file.readlines()))
#     c = filter(lambda x: len(x) == max(map(len, a)), a)
#     for i in c: print(i)


# Сумма чисел в строках
# with open(r'C:\Users\n.kan\numbers.txt', encoding='utf-8') as file:
#     a = list(map(lambda x: x.rstrip().split(), file.readlines()))
#     for i in a:
#         i = list(map(int, i))
#         print(sum(i))


# Сумма чисел в файле
# with open(r'C:\Users\n.kan\nums.txt', encoding='utf-8') as file:
#     a = list(map(lambda x: x.rstrip().split(), file.readlines()))
#     q = []
#     for i in a:
#         for j in i:
#             f = list(map(lambda x: x if x.isdigit() else ' ', j))
#             s1 = "".join(f)
#             for l in s1.split():
#                 q.append(l)
# q = list(map(int, q))
# print(sum(q))


# Статистика по файлу
# with open(r'C:\Users\n.kan\file.txt', encoding='utf-8') as file:
#     a = list(map(lambda x: x.rstrip().split(), file.readlines()))
#     lines, words, letters = len(a), 0, 0
#     for i in a:
#         for j in i:
#             words += 1
#             for k in j:
#                 if k.isalpha():
#                     letters += 1
#
# result = f'Input file contains:\n{letters} letters\n{words} words\n{lines} lines'
# print(result)

#  Random name and surname
# import random
# with open(r'C:\Users\n.kan\last_names.txt', encoding='utf-8') as surname, open(r'C:\Users\n.kan\first_names.txt', encoding='utf-8') as name:
#     surnameList = list(map(lambda x: x.rstrip(), surname.readlines()))
#     nameList = list(map(lambda x: x.rstrip(), name.readlines()))
#     n = 3 # Количество рандомных итераций
#     for i, j in zip(random.sample(nameList, n),random.sample(surnameList, n)):
#         print(i, j)


# Необычные страны
# with open(r'C:\Users\n.kan\population.txt', encoding='utf-8') as cityRoster:
#     cilyList = list(map(lambda x: x.rstrip().split('\t'), cityRoster.readlines()))
#     cilyList = filter(lambda x, y: x[0].startswith('G') and int(x[1]) > 500000, *cilyList)
#     for i, j in cilyList:
#         print(i)


# CSV-файл
# def read_csv():
#     with open(r'C:\Users\n.kan\data.csv', encoding='utf-8') as csvfile:
#         a = csvfile.readline().strip().split(',')
#         fullDict = []
#         for line in csvfile:
#             temp = {}
#             for i, j in zip(a, line.strip().split(',')):
#                 temp[i] = j
#             fullDict.append(temp)
#     return fullDict


# Метод write()
# with open('myfile.txt', 'r+', encoding='utf-8') as file:
#     file.write('Python and beegeek forever\n')
#     file.write('We love stepik.')
# Метод writelines()
# with open('philosophers.txt', 'w', encoding='utf-8') as file:
#     file.write('Джoн Локк\n')
#     file.write('Дэвид Хьюм\n')
#     file.write('Эдмyнд Берк\n')

# Запись в файл с помощью функции print()
# with open('philosophers.txt', 'w', encoding='utf-8') as output:
#     print('Джoн Локк', file=output)
#     print('Дэвид Хьюм', file=output)
#     print('Эдмyнд Берк', file=output)

# Случайные числа
# import random as r
# with open('random.txt', 'w', encoding='utf-8') as output:
#     a = [str(r.randint(111, 777)) + '\n' for _ in range(25)]
#     print(a)
#     print(len(a))
#     output.writelines(a)

# Нумерация строк

# with open(r'C:\Users\n.kan\input.txt', 'r', encoding='utf-8') as inputFile, open(r'C:\Users\n.kan\output.txt', 'w', encoding='utf-8') as outputFile:
#     n = 1
#     for inline in inputFile:
#         outline = f'{n}) {inline}'
#         outputFile.write(outline)
#         n += 1

# Подарок на новый год
# with open(r'C:\Users\n.kan\class_scores.txt', 'r', encoding='utf-8') as inputFile, open(r'C:\Users\n.kan\new_scores.txt', 'w', encoding='utf-8') as outputFile:
#     for inline in inputFile:
#         name, score = inline.split()
#         scoreInt = int(score) + 5 if int(score) + 5 <= 100 else 100
#         outline = f'{name} {scoreInt}\n'
#         outputFile.write(outline)


# Загадка от Жака Фреско 🌶️
# with open(r'C:\Users\n.kan\goats.txt', 'r', encoding='utf-8') as goatsFile:
#     raw = [i.strip() for i in goatsFile.readlines()]
#
#
# exampleGoats = raw[1: raw.index('GOATS')]
# unsortedGoats = raw[raw.index('GOATS') + 1:]
# goatCount = {goat: unsortedGoats.count(goat) for goat in exampleGoats}
# sumOfGoats = sum(goatCount.values())
# toOutputList = []
# for goat, goatValue in goatCount.items():
#     if goatValue * 100 / sumOfGoats > 7:
#         toOutputList.append(goat + '\n')
# with open(r'C:\Users\n.kan\answer.txt', 'w', encoding='utf-8') as answerFile:
#     answerFile.writelines(sorted(toOutputList))

# Конкатенация файлов 🌶️
# n = int(input())
# b = [input() for _ in range(n)]
# with open('output.txt', 'w', encoding='utf-8') as goal:
#     for i in b:
#         with open(i, 'r', encoding='utf-8') as temp:
#             goal.write(temp.read())

# Лог файл 🌶️
# with open(r'C:\Users\n.kan\logfile.txt', 'r', encoding='utf-8') as rawFile:
#     raw = list(map(lambda x: x.rstrip().split(','), rawFile.readlines()))
#
#
# def timeToEpoch(time):
#     hour, minute = time.split(':')
#     epoch = int(hour) * 60 + int(minute)
#     return epoch
#
#
# toOutput = []
# for i in raw:
#     [name, timeIn, timeOut] = i
#     if timeToEpoch(timeOut) - timeToEpoch(timeIn) >= 60:
#         toOutput.append(name + '\n')
#
#
# with open(r'C:\Users\n.kan\output.txt', 'w', encoding='utf-8') as rawFile:
#     rawFile.writelines(toOutput)


# with open('ledger.txt', 'r', encoding='utf-8') as rawFile:
#     s = 0
#     for line in rawFile:
#         lineInt = line.strip()[1:]
#         s += int(lineInt)
# print(f'${s}')

# Goooood students
# with open('grades.txt', 'r', encoding='utf-8') as rawFile:
#     s = 0
#     for line in rawFile:
#         lineList = line.strip().split()
#         [name, grade1, grade2, grade3] = lineList
#         if int(grade1) >= 65 and int(grade2) >= 65 and int(grade3) >= 65:
#             s += 1
# print(s)


# Самое длиное слово в файле

# with open('words.txt', 'r', encoding='utf-8') as rawFile:
#     s = []
#     for line in rawFile:
#         lineList = line.strip().split()
#         for word in lineList:
#             s.append(word)
#
# toLenght = list(map(len, s))
# maxLen = max(toLenght)
# toPrintList = filter(lambda x: len(x) == maxLen, s)
# for i in toPrintList:
#     print(i)

# Tail of a File
# with open(input(), 'r', encoding='utf-8') as rawFile:
#     linesList = rawFile.readlines()
#     for line in linesList[-10:]:
#         print(line.strip())

# Forbidden words 🌶️
# with open('forbidden_words.txt', 'r', encoding='utf-8') as rawFile:
#     forbiddenDict = {}
#     for line in rawFile:
#         linesList = line.strip().split()
#         for word in linesList:
#             forbiddenDict[word] = '*' * len(word)
# with open(input(), 'r', encoding='utf-8') as originalFile:
#     moderatedList = []
#     for line in originalFile:
#         # print(line.strip())
#         anyCaseStr = line.strip()
#         lowCaseStr = anyCaseStr.lower()
#         # print(lowCaseStr)
#         for key,value in forbiddenDict.items():
#             numberForbiidenWord = lowCaseStr.count(key)
#             lowCaseStr = lowCaseStr.replace(key,value,numberForbiidenWord)
#         print(lowCaseStr)

# with open(input()) as file, open('forbidden_words.txt') as forbidden_words:
#     f_words = {f_word.strip(): len(f_word.strip()) for f_word in forbidden_words.read().split()}
#     text = file.read()
#     for f_word, lenght in f_words.items():
#         while f_word in text.lower():
#             index = text.lower().find(f_word)
#             text = text[:index] + ('*' * lenght) + text[index + lenght:]
#     print(text)


# a = 'asdfasdfasdfadsf234234asdf'
# s = a.count('asdf')
# z = a.replace('asdf','****',s)
# print(z)



a = 100
b = 3
print(type(100 // 3))