#  https://stepik.org/lesson/482377/step/1?after_pass_reset=true&auth=login&unit=473680
#  https://docs.python.org/3/reference/expressions.html#operator-precedence
#  Объединение множеств: метод union()
# myset1 = {1, 2, 3, 4, 5}
# myset2 = {3, 4, 6, 7, 8}
#
# myset3 = myset1.union(myset2)
# print(myset3)
#
#
# myset1 = {1, 2, 3, 4, 5}
# myset2 = {3, 4, 6, 7, 8}
#
# myset3 = myset1 | myset2
# print(myset3)
#
# #  Пересечение множеств: метод intersection()
#
# myset1 = {1, 2, 3, 4, 5}
# myset2 = {3, 4, 6, 7, 8}
#
# myset3 = myset1.intersection(myset2)
# print(myset3)
#
# myset1.intersection_update({3, 4, 6, 7, 8})
# print(myset1)
#
# myset1 = {1, 2, 3, 4, 5}
# myset2 = {3, 4, 6, 7, 8}
#
# myset3 = myset1 & myset2
# print(myset3)
#
# #  Разность множеств: метод difference()
# myset1 = {1, 2, 3, 4, 5}
# myset2 = {3, 4, 6, 7, 8}
#
# myset3 = myset1.difference(myset2)
# print(myset3)
#
# myset1 = {1, 2, 3, 4, 5}
# myset2 = {3, 4, 6, 7, 8}
#
# myset3 = myset1 - myset2
# print(myset3)
#
# myset1 = {1, 2, 3, 4, 5}
# myset2 = {3, 4, 6, 7, 8}
#
# myset3 = myset2.difference(myset1)
# print(myset3)
#
# #  Симметрическая разность: метод symmetric_difference()
#
# myset1 = {1, 2, 3, 4, 5}
# myset2 = {3, 4, 6, 7, 8}
#
# myset3 = myset1.symmetric_difference(myset2)
# print(myset3)
#
# myset1 = {1, 2, 3, 4, 5}
# myset2 = {3, 4, 6, 7, 8}
#
# myset3 = myset1 ^ myset2
# print(myset3)
#
#
# #  Методы множеств, изменяющие текущие множества
# # Метод update() изменяет исходное множество по объединению.
# myset1 = {1, 2, 3, 4, 5}
# myset2 = {3, 4, 6, 7, 8}
#
# myset1.update(myset2)      # изменяем множество myset1
# print(myset1)
#
# myset1 = {1, 2, 3, 4, 5}
# myset2 = {3, 4, 6, 7, 8}
#
# myset1 |= myset2
# print(myset1)
#
# #  Метод intersection_update() изменяет исходное множество по пересечению.
# print('Метод intersection_update')
# myset1 = {1, 2, 3, 4, 5}
# myset2 = {3, 4, 6, 7, 8}
#
# myset1.intersection_update(myset2)      # изменяем множество myset1
# print(myset1)
#
# myset1 = {1, 2, 3, 4, 5}
# myset2 = {3, 4, 6, 7, 8}
#
# myset1 &= myset2
# print(myset1)
#
# # Метод difference_update() изменяет исходное множество по разности.
#
# myset1 = {1, 2, 3, 4, 5}
# myset2 = {3, 4, 6, 7, 8}
#
# myset1.difference_update(myset2)      # изменяем множество myset1
# print(myset1)
#
# myset1 = {1, 2, 3, 4, 5}
# myset2 = {3, 4, 6, 7, 8}
#
# myset1 -= myset2
# print(myset1)
#
# #  Метод symmetric_difference_update() изменяет исходное множество по симметрической разности.
# myset1 = {1, 2, 3, 4, 5}
# myset2 = {3, 4, 6, 7, 8}
#
# myset1.symmetric_difference_update(myset2)      # изменяем множество myset1
# print(myset1)
#
# myset1 = {1, 2, 3, 4, 5}
# myset2 = {3, 4, 6, 7, 8}
#
# myset1 ^= myset2
# print(myset1)

# Количество совпадающих
# a = set(input().split())
# b = set(input().split())
# print(len(a.intersection(b)))

# Числа первой строки
# a = input().split()
# b = input().split()
# x = set()
# y = set()
# for i in range(len(a)):
#     a[i] = int(a[i])
#     x.add(a[i])
# for i in range(len(b)):
#     b[i] = int(b[i])
#     y.add(b[i])
#
# c = sorted(x - y)
# print(*c)


# s = set()
# n = int(input())
# a = [set(int(i) for i in input()) for j in range(n)]
# b = set()
# b =b | a[0]
# for i in a:
#     b = b & i
# c = sorted(b)
# print(*c)


# Методы issuperset(), issubset(), isdisjoint()
# Сравнение множеств (<, >, <=, >=)


set1 = {2, 3}
set2 = {1, 2, 3, 4, 5, 6}

print(set1.issubset(set2))

set1 = {2, 3}
set2 = {1, 2, 3, 4, 5, 6}

print(set1 <= set2)


# Метод issuperset()

set1 = {'a', 'b', 'c', 'd', 'e'}
set2 = {'c', 'e'}

print(set1.issuperset(set2))

set1 = {'a', 'b', 'c', 'd', 'e'}
set2 = {'c', 'e'}

print(set1 >= set2)

#  Метод isdisjoint()
set1 = {1, 2, 3, 4, 5}
set2 = {5, 6, 7}
set3 = {7, 8, 9}

print(set1.isdisjoint(set2))
print(set1.isdisjoint(set3))
print(set2.isdisjoint(set3))

# Примечание 1. Методы issuperset(), issubset(), isdisjoint() могут принимать в качестве аргумента не только
# множество (тип данных set), но и любой итерируемый объект (список, строку, кортеж).
#
# Примечание 2. Операторы >, <, >=, <= требуют наличия в качестве операндов множеств.


word = 'beegeek'
set1 = set(word*3)
set2 = set(word[::-1]*2 + 'stepik')
print(set1)
print(set2)


a = set([int(num) for num in input().split()])
b = set([int(num) for num in input().split()])
c = set([int(num) for num in input().split()])
e = set(range(11))
d = e - (a | b | c)
k = sorted(d)
print(*k)