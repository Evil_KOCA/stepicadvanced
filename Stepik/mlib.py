def tr(po):  # Транспонирование относительно главной диагонали
    ni = len(po)
    nj = len(po[0])
    z = [[0 for i in range(ni)] for j in range(nj)]
    for ii in range(ni):
        for j in range(nj):
            z[j][ii] = po[ii][j]
    return z


def trorb(po):  # Транспонирование относительно побочной диагонали
    ni = len(po)
    nj = len(po[0])
    z = [[0 for i in range(ni)] for j in range(nj)]
    for i in range(ni):
        for j in range(nj):
            z[j][i] = po[n - 1 - i][n - 1 - j]
    return z


def swapkol(o1, o2, po):
    buf = [0 for _ in range(len(po[0]))]
    for i in range(len(po[o2])):
        buf[i] = po[o2][i]
    for i in range(len(po[o1])):
        po[o2][i] = po[o1][i]
    for i in range(len(po[o1])):
        po[o1][i] = buf[i]
    return po


def printmat(a):
    ni = len(a[0])
    nj = len(a)
    for ii in range(nj):
        for jj in range(ni):
            print(str(a[ii][jj]).ljust(3), end=" ")
        print()
    print()


def summ(po1, po2):
    po1x = len(po1)
    po1y = len(po1[0])
    po2x = len(po2)
    po2y = len(po2[0])
    if po1x == po2x or po1y == po2y:  # число строк первой матрицы равно числу столбцов второй
        po3 = [[0 for i in range(po1y)] for j in range(po1x)]
        for i in range(po1x):
            for j in range(po1y):
                po3[i][j] = po1[i][j] + po2[i][j]
        return po3
    elif po1x != po2x or po1y != po2y:
        print('Матрицы НЕЛЬЗЯ СЛОЖИТЬ')


def multi(po1, po2):
    po1x = len(po1)
    po1y = len(po1[0])
    po2x = len(po2)
    po2y = len(po2[0])
    if po1y == po2x:  # число строк первой матрицы равно числу столбцов второй
        po3 = [[0 for i in range(po2y)] for j in range(po1x)]
        for i in range(po2y):
            for j in range(po1x):
                for k in range(po1y):
                    po3[i][j] += po1[i][k] * po2[k][j]
        return po3
    elif po1y != po2x:
        print('Матрицы НЕЛЬЗЯ перемножить')


def power(k, po):
    po1 = [[0 for i in range(len(po[0]))] for j in range(len(po))]
    for i in range(len(po)):
        for j in range(len(po[0])):
            po1[i][j] = po[i][j]
    for i in range(1, k):
        po1 = multi(po1, po)
    return po1
