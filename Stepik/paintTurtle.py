import turtle
import time

# turtle.shape('square')
# turtle.forward(100)
# turtle.setheading(90)
#
# turtle.shape('arrow')
# turtle.forward(100)
# turtle.setheading(180)
#
# turtle.shape('turtle')
# turtle.forward(100)
# turtle.setheading(270)
#
# turtle.shape('circle')
# turtle.forward(100)
#



def rectangle(* args):
    turtle.shape('turtle')
    for i in args * 2:
        turtle.forward(i)
        turtle.left(90)

def squareside(n):
    for j in range(4):
        turtle.forward(n)
        turtle.left(90)


def hexagon(side):
    for i in range(6):
        turtle.forward(side)
        turtle.right(60)


def mirror(side):
    turtle.forward(side)
    turtle.left(60)
    hexagon(side)


def romb(side, angle):
    for i in range(2):
        turtle.forward(side)
        turtle.left(angle)
        turtle.forward(side)
        turtle.left(180 - angle)


def snowstar(side, n):
    alp = 360 / n
    for i in range(n):
        turtle.forward(side)
        turtle.backward(side)
        turtle.left(alp)


def fivekornerstar(side):
    alp = 72
    turtle.left(alp)
    for i in range(3):
        turtle.forward(side)
        turtle.right(alp * 2)
        turtle.forward(side)
        turtle.right(alp * 2)




# rectangle(100, 40)
# for i in range(4):
#     turtle.left(10)
#     squareside(100)

for i in range(19):
    squareside(100 - 5 * i)
time.sleep(3)
turtle.done
