# Каждый n-ый элемент
# a = input().split()
# n = int(input())
#
# k = []
# for i in range(n):
#     k += [[]]
#
# for i in range(len(a)):
#     k[i % n].append(a[i])
# print(k)


# Максимальный в области 2
# n = int(input())
# a = [[int(num) for num in input().split()] for i in range(n)]
#
#
# m = a[n-1][n-1]
# for i in range(n):
#     for j in range(n):
#         if i >= n - 1 - j:
#             if a[i][j] > m:
#                 m = a[i][j]
# print(m)


# Транспонирование матрицы

# def tr(po):
#     ni = len(po)
#     nj = len(po[0])
#     z = [[0 for i in range(ni)] for j in range(nj)]
#     for ii in range(ni):
#         for j in range(nj):
#             z[j][ii] = po[ii][j]
#     return z
#
#
# n = int(input())
# a = [[int(num) for num in input().split()] for i in range(n)]
# b = tr(a)
# for i in b:
#     for j in i:
#         print(j, end=' ')
#     print()


# Снежинка

# n = int(input())
# a = [['.' for i in range(n)] for j in range(n)]
#
#
# k = n // 2
#
#
# for i in range(n):
#     for j in range(n):
#         if i == j or i == n - 1 - j or k - 1 <= i <= k + 1 and k - 1 <= j <= k + 1 or i == k or j == k:
#             a[i][j] = '*'
# for i in a:
#     for j in i:
#         print(j, end=' ')
#     print()

# Симметричная матрица
# def trorb(po):
#     ni = len(po)
#     nj = len(po[0])
#     z = [[0 for i in range(ni)] for j in range(nj)]
#     for i in range(ni):
#         for j in range(nj):
#             z[j][i] = po[n - 1 - i][n - 1 - j]
#     return z
#
#
# n = int(input())
# a = [[int(num) for num in input().split()] for i in range(n)]
# b = trorb(a)
#
#
# if a == b:
#     print('YES')
# elif a != b:
#     print('NO')

# Латинский квадрат 🌶️

#
# def tr(po):  # Транспонирование относительно главной диагонали
#     ni = len(po)
#     nj = len(po[0])
#     z = [[0 for i in range(ni)] for j in range(nj)]
#     for ii in range(ni):
#         for j in range(nj):
#             z[j][ii] = po[ii][j]
#     return z
#
#
# n = int(input())
# a = [[int(num) for num in input().split()] for i in range(n)]
# b = tr(a)
#
# k = []
# s = set()
# q = set()
# for i in range(1, n + 1):
#     s.add(i)
#
# for i in a:
#     for j in i:
#         q.add(j)
#     k.append(s == q)
#     q.clear()
#
# for i in b:
#     for j in i:
#         q.add(j)
#     k.append(s == q)
#     q.clear()
#
# if False in k:
#     print('NO')
# else:
#     print('YES')


# Ходы ферзя
# n = 8 # ПОЛЕ
# m = [['.' for i in range(n)] for j in range(n)]
# a = [i for i in range(8)]
# b = [i for i in 'abcdefgh']
# d = {key: value for key, value in zip(b, a)}
#
# a = input() # Ввод поля, где стоит конь
# y = d[a[0]]
# x = n - int(a[1])
#
# m[x][y] = 'Q'
# for i in range(n):
#     for j in range(n):
#         if m[i][j] == 'Q':
#             for di in range(-7, 8):
#                 for dj in range(-7, 8):
#                     f = i + di
#                     g = j
#                     if 0 <= f <= n - 1 and 0 <= g <= n - 1:
#                         m[f][g] = '*'
#                 for dj in range(-7, 8):
#                     f = i
#                     g = j + dj
#                     if 0 <= f <= n - 1 and 0 <= g <= n - 1:
#                         m[f][g] = '*'
#                 f = i + di
#                 g = j + di
#                 if 0 <= f <= n - 1 and 0 <= g <= n - 1:
#                     m[f][g] = '*'
#                 f = i + di
#                 g = j - di
#                 if 0 <= f <= n - 1 and 0 <= g <= n - 1:
#                     m[f][g] = '*'
#
# m[x][y] = 'Q'
# for i in m:
#     print(*i)


# Диагонали параллельные главной


def tr(po):  # Транспонирование относительно главной диагонали
    ni = len(po)
    nj = len(po[0])
    z = [[0 for i in range(ni)] for j in range(nj)]
    for ii in range(ni):
        for j in range(nj):
            z[j][ii] = po[ii][j]
    return z


def summ(po1, po2):
    po1x = len(po1)
    po1y = len(po1[0])
    po2x = len(po2)
    po2y = len(po2[0])
    if po1x == po2x or po1y == po2y:  # число строк первой матрицы равно числу столбцов второй
        po3 = [[0 for i in range(po1y)] for j in range(po1x)]
        for i in range(po1x):
            for j in range(po1y):
                po3[i][j] = po1[i][j] + po2[i][j]
        return po3
    elif po1x != po2x or po1y != po2y:
        print('Матрицы НЕЛЬЗЯ СЛОЖИТЬ')


n = int(input())

a = [[0 for i in range(n)] for j in range(n)]

for i in range(n):
    for j in range(n):
        if i >= j:
            a[i][j] = i - j


b = tr(a)
c = summ(a, b)
for i in c:
    for j in i:
        print(j, end=' ')
    print()
