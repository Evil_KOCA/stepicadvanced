# words = ['xyz', 'zara', 'beegeek']
# print(max(words))
# list1 = [10, 20, [300, 400, [5000, 6000], 500], 30, 40]
# a = [10, 20, [300, 400, [5000, 6000, 7000], 500], 30, 40]
# list1[2][2].append(7000)
# print(list1)
#
#
# a = ['a', 'b']
# q = [[a[0]]]
# q[0].append(a[1])
# q.extend([a])
# print(q)
#
# q = [['s']]
# z = q.append(['sdf'])
# print(q[-1].append(['asdf']))
# print(z)
#
# a = ['a', 'b']
# a.insert(0, 'c')
# print(a)
#
# def matrix(n = 1, m = None, value = 0):
#     if m == None:
#         m = n
#     s = [[value for i in range(m)] for j in range(n)]
#     return s
# print(matrix())
# print(matrix(3))
# print(matrix(2, 5))
# print(matrix(3,4,9))
#
#
# def my_func(a, b, *args, name='Gvido', age=17, **kwargs):
#     print(a, b)
#     print(args)
#     print(name, age)
#     print(kwargs)
#
# print()
# my_func(1, 2, 3, 4, name='Timur', age=28, job='Teacher', language='Python')
# print()
# my_func(1, 2, name='Timur', age=28, job='Teacher', language='Python')
# print()
# my_func(1, 2, 3, 4, job='Teacher', language='Python')
#
# def asdf(*args):
#     print(args)
#     return max(args) + min(args)
#
#
# print(asdf(10, 15, *[31, 42, 5, 1], *(17, 28, 19, 100), 13, 12))


# def count_args(*args):
#     return len(args)
#
#
# def sq_sum(*args):
#     s = 0
#     for i in args:
#         s += i ** 2
#     return s
#
#
# def mean(*args):
#     s = 0
#     n = 0
#     for i in args:
#         if type(i) in (int, float):
#             s, n = s + i, n + 1
#     if n == 0:
#         return 0.0
#     else:
#         return s / n
#
#
# def greet(name, *args):
#     return f'Hello, {" and ".join((name,) + args)}!'
# print(greet('Timur', 'Roman', 'Ruslan'))
# print(greet('Timur'))

# def print_products(*args):
#     s = []
#     for i in args:
#         if type(i) == str and i !='':
#             s += [i]
#     if len(s) != 0:
#         for i in range(len((s))):
#             print(str(i + 1) + ') ' + s[i])
#     else:
#         print('Нет продуктов')
#
# print_products('Бананы', [1, 2], ('Stepik',), 'Яблоки', '', 'Макароны', 5, True)
# print_products([4], {}, 1, 2, {'Beegeek'}, '')

# def info_kwargs(**kwargs):
#     z = sorted(kwargs)
#     for i in z:
#         print(f'{i}: {kwargs[i]}')
#
#
# info_kwargs(first_name='Timur', last_name='Guev', age=28, job='teacher')
# print()
# info_kwargs(name='Timur', surname='Guev', language='Python', age=28, job='programer', country='Russia', students=[1, 2, 3, 4, 5], isflag=True)
#
#
# words = ['this', 'test', 'bask']
# words.sort(key=len)
# print(words)

# numbers = [(10, 10, 10), (30, 45, 56), (81, 39), (1, 2, 3), (12,), (-2, -4, 100), (1, 2, 99), (89, 9, 34), (10, 20, 30, -2), (50, 40, 50), (34, 78, 65), (-5, 90, -1, -5), (1, 2, 3, 4, 5, 6), (-9, 8, 4), (90, 1, -45, -21)]
#
# def compare(po):
#     a = sum(po) / len(po)
#     return a
#
# print(min(numbers, key=compare))
# print(max(numbers, key=compare))


# points = [(-1, 1), (5, 6), (12, 0), (4, 3), (0, 1), (-3, 2), (0, 0), (-1, 3), (2, 0), (3, 0), (-9, 1), (3, 6), (8, 8)]
# def ans(po):
#     l = (po[0] ** 2 + po[1] ** 2) ** 0.5
#     return l
# points.sort(key=ans)
# print(points)


# numbers = [(10, 10, 10), (30, 45, 56), (81, 80, 39), (1, 2, 3), (12, 45, 67), (-2, -4, 100), (1, 2, 99), (89, 90, 34),
#            (10, 20, 30), (50, 40, 50), (34, 78, 65), (-5, 90, -1)]
#
#
# def ans(po):
#     l = min(po) + max(po)
#     return l
#
#
# numbers.sort(key=ans)
# print(numbers)

# athletes = [('Дима', 10, 130, 35), ('Тимур', 11, 135, 39), ('Руслан', 9, 140, 33), ('Рустам', 10, 128, 30),
#             ('Амир', 16, 170, 70), ('Рома', 16, 188, 100), ('Матвей', 17, 168, 68), ('Петя', 15, 190, 90)]

#
# def f1(po):
#     return po[0]
#
#
# def f2(po):
#     return po[1]
#
#
# def f3(po):
#     return po[2]
#
#
# def f4(po):
#     return po[3]
#
#
# f = [1, f1, f2, f3, f4]
#
# n = int(input())
# athletes.sort(key=f[n])
# for i in athletes:
#     print(*i)


# from math import sin
# square = lambda i: i ** 2
# cube = lambda i:  i ** 3
# sqrt = lambda i: i ** .5
# di = {"квадрат": square, "куб": cube, "корень": sqrt, "модуль": abs, "синус": sin}
# n = int(input())
# print(di[input()](n))



# import math
#
#
# q = {'квадрат': 1, 'куб': 2, 'корень': 3, 'модуль': 4,
#      'синус': 5}
#
#
# def por(x, po):
#     l = [x ** 2, x ** 3, x ** 0.5, abs(x), math.sin(x)]
#     return l[po - 1]
#
#
# n = int(input())
# k = input()
# print(por(n, q[k]))

# Интересная сортировка-1
# def ans(stri):
#     k = 0
#     for i in stri:
#         k += int(i)
#     return k
#
# q = input().split()
#
# q.sort(key=ans)
# print(*q)


# Интересная сортировка-2
def ans(stri):
    k = 0
    for i in str(stri):
        k += int(i)
    return k

q = [int(i) for i in input().split()]
q.sort()
q.sort(key=ans)
print(*q)

# def high_order_function(func):     # функция высшего порядка, так как принимает функцию
#     return func(3)
#
#
# def double(x):                     # обычная функция = функция первого порядка
#     return 2*x
#
#
# def add_one(x):                    # обычная функция = функция первого порядка
#     return x + 1
#
# print(high_order_function(double))
# print(high_order_function(add_one))
#
#
# def f(x):
#     return x**2     # тело функции, которая преобразует аргумент x
#
#
# old_list = [1, 2, 4, 9, 10, 25]
# new_list = []
# for item in old_list:
#     new_item = f(item)
#     new_list.append(new_item)
#
# print(old_list)
# print(new_list)


# def map(function, items):
#     result = []
#     for item in items:
#         new_item = function(item)
#         result.append(new_item)
#     return result
#
#
# def square(x):
#     return x**2
#
#
# def cube(x):
#     return x**3
#
#
# numbers = [1, 2, -3, 4, -5, 6, -9, 0]
#
# strings = map(str, numbers)        # используем в качестве преобразователя - функцию str
# abs_numbers = map(abs, numbers)    # используем в качестве преобразователя - функцию abs
#
# squares = map(square, numbers)     # используем в качестве преобразователя - функцию square
# cubes = map(cube, numbers)         # используем в качестве преобразователя - функцию cube
#
#
# strings = ['10', '12', '-4', '-9', '0', '1', '23', '100', '99']
#
# numbers1 = [int(str) for str in strings]   # используем списочное выражение для преобразования
# numbers2 = map(int, strings)               # используем функцию map() для преобразования
#
# print(numbers1)
# print(numbers2)

# numbers = [3.56773, 5.57668, 4.00914, 56.24241, 9.01344, 32.12013, 23.22222, 90.09873, 45.45, 314.1528, 2.71828, 1.41546]
#
# num = list(map(round, numbers, [2] * len(numbers)))
# print(num)

# def map(function, items):
#     result = []
#     for item in items:
#         result.append(function(item))
#     return result
#
#
# def filter(function, items):
#     result = []
#     for item in items:
#         if function(item):
#             result.append(item)
#     return result
#
#
# numbers = [77, 293, 28, 242, 213, 285, 71, 286, 144, 276, 61, 298, 280, 214, 156, 227, 228, 51, -4, 202, 58, 99, 270, 219, 94, 253, 53, 235, 9, 158, 49, 183, 166, 205, 183, 266, 180, 6, 279, 200, 208, 231, 178, 201, 260, -35, 152, 115, 79, 284, 181, 92, 286, 98, 271, 259, 258, 196, -8, 43, 2, 128, 143, 43, 297, 229, 60, 254, -9, 5, 187, 220, -8, 111, 285, 5, 263, 187, 192, -9, 268, -9, 23, 71, 135, 7, -161, 65, 135, 29, 148, 242, 33, 35, 211, 5, 161, 46, 159, 23, 169, 23, 172, 184, -7, 228, 129, 274, 73, 197, 272, 54, 278, 26, 280, 13, 171, 2, 79, -2, 183, 10, 236, 276, 4, 29, -10, 41, 269, 94, 279, 129, 39, 92, -63, 263, 219, 57, 18, 236, 291, 234, 10, 250, 0, 64, 172, 216, 30, 15, 229, 205, 123, -105]
#
# def ar(po):
#     return 9 < abs(po) < 100 and po % 7 == 0
#
# def cube(x):
#     return x ** 2
#
# b = filter(ar, numbers)
# print(b)
# print(sum(map(cube, filter(ar, numbers))))


# def map(function, items):
#     result = []
#     for item in items:
#         result.append(function(item))
#     return result
#
#
# def add3(x):
#     return x + 3
#
#
# def mul7(x):
#     return x * 7
#
#
# def func_apply(func, po):
#     s = map(func, po)
#     return s
#
#
#
# print(func_apply(mul7, [1, 2, 3, 4, 5, 6]))
# print(func_apply(add3, [1, 2, 3, 4, 5, 6]))
# print(func_apply(str, [1, 2, 3, 4, 5, 6]))


# !!!!!!!!!!!!!!!!!
# def increase(num):
#     return num + 7
#
#
# numbers = [1, 2, 3, 4, 5, 6]
# new_numbers = map(increase, numbers)     #  используем встроенную функцию map()
#
# print(new_numbers)
#
#
# def func(elem1, elem2, elem3):
#     return elem1 + elem2 + elem3
#
#
# numbers1 = [1, 2, 3, 4, 5]
# numbers2 = [10, 20, 30, 40, 50]
# numbers3 = [100, 200, 300, 400, 500]
#
# new_numbers = list(map(func, numbers1, numbers2, numbers3))  #  преобразуем итератор в список
#
# print(new_numbers)
#
#
# def func(elem1, elem2, elem3):
#     return elem1 + elem2 + elem3
#
#
# numbers1 = [1, 2, 3, 4]
# numbers2 = [10, 20]
# numbers3 = [100, 200, 300, 400, 500]
#
# new_numbers = list(map(func, numbers1, numbers2, numbers3))  #  преобразуем итератор в список
#
# print(new_numbers)
#
# print()
# circle_areas = [3.56773, 5.57668, 4.31914, 6.20241, 91.01344, 32.01213]
#
# result1 = list(map(round, circle_areas, [1]*6))         # округляем числа до 1 знака после запятой
# result2 = list(map(round, circle_areas, range(1, 7)))   # округляем числа до 1,2,...,6 знаков после запятой
#
# print(circle_areas)
# print(result1)
# print(result2)

#
# def func(elem):
#     return elem >= 0
#
#
# numbers = [-1, 2, -3, 4, 0, -20, 10]
# positive_numbers = list(filter(func, numbers))  #  преобразуем итератор в список
#
# print(positive_numbers)
#
#
# true_values = filter(None, [1, 0, 10, '', None, [], [1, 2, 3], ()])
#
# for value in true_values:
#     print(value)
#
#
# from functools import reduce
#
#
# def func(a, b):
#     return a + b
#
#
# numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
# total = reduce(func, numbers)   # в качестве начального значения 0
# print(total)
#
#
#
# import operator
#
# words = ['Testing ', 'shows ', 'the ', 'presence', ', ', 'not ', 'the ', 'absence ', 'of ', 'bugs']
# numbers = [1, 2, -6, -4, 3, 9, 0, -6, -1]
#
# opposite_numbers = list(map(operator.neg, numbers))    #  смена знаков элементов списка
# concat_words = reduce(operator.add, words)             #  конкатенация элементов списка
#
# print(opposite_numbers)
# print(concat_words)
#
# pets = ['alfred', 'tabitha', 'william', 'arla']
# chars = ['x', 'y', '2', '3', 'a']
#
# uppered_pets = list(map(str.upper, pets))
# capitalized_pets = list(map(str.capitalize, pets))
# only_letters = list(filter(str.isalpha, chars))
#
# print(uppered_pets)
# print(capitalized_pets)
# print(only_letters)
#
# random_list = [1, 'a', 0, False, True, '0', 7, '']
# filtered_list = list(filter(None, random_list))
# print(filtered_list)