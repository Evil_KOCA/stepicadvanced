# Тема урока: функции any(), all(), zip(), enumerate()
# Функции all() и any()
# Функция all()
# print(all([True, True, True]))     #  возвращает True, так как все значения списка равны True
# print(all([True, True, False]))    #  возвращает False, так как не все значения списка равны True
# print()
# print(all([1, 2, 3]))
# print(all([1, 2, 3, 0, 5]))
# print(all([True, 0, 1]))
# print(all(('', 'red', 'green')))
# print(all({0j, 3+4j}))
# print()
#
# dict1 = {0: 'Zero', 1: 'One', 2: 'Two'}
# dict2 = {'Zero': 0, 'One': 1, 'Two': 2}
#
# print(all(dict1))
# print(all(dict2))
#
#
# print(all([]))          #  передаем пустой список
# print(all(()))          #  передаем пустой кортеж
# print(all(''))          #  передаем пустую строку
# print(all([[], []]))    #  передаем список, содержащий пустые списки
#
# # Функция any()
# print(any([False, True, False]))       #  возвращает True, так как есть хотя бы один элемент, равный True
# print(any([False, False, False]))      #  возвращает False, так как нет элементов, равных True
# print()
#
# print(any([0, 0, 0]))
# print(any([0, 1, 0]))
# print(any([False, 0, 1]))
# print(any(['', [], 'green']))
# print(any({0j, 3+4j, 0.0}))
#
# print()
# dict1 = {0: 'Zero'}
# dict2 = {'Zero': 0, 'One': 1}
#
# print(any(dict1))
# print(any(dict2))
#
# print()
# print(any([]))          #  передаем пустой список
# print(any(()))          #  передаем пустой кортеж
# print(any(''))          #  передаем пустую строку
# print(any([[], []]))    #  передаем список, содержащий пустые списки
#
# # Функции all() и any() в связке с функцией map()
# print()
# numbers = [17, 90, 78, 56, 231, 45, 5, 89, 91, 11, 19]
#
# result = all(map(lambda x: x > 10, numbers))
#
# if result:
#     print('Все числа больше 10')
# else:
#     print('Хотя бы одно число меньше или равно 10')
#
# print()
# numbers = [17, 91, 78, 55, 231, 45, 5, 89, 99, 11, 19]
#
# result = any(map(lambda x: x % 2 == 0, numbers))
#
# if result:
#     print('Хотя бы одно число четное')
# else:
#     print('Все числа нечетные')
# print()
#
# # Функция enumerate()
# colors = ['red', 'green', 'blue']
#
# for pair in enumerate(colors):
#     print(pair)
# print(list(enumerate(colors)))
#
# colors = ['red', 'green', 'blue']
#
# print()
# pairs = enumerate(colors)
#
# print(pairs)
# print(list(pairs))
#
# print()
# colors = ['red', 'green', 'blue']
# for index, item in enumerate(colors):
#     print(index, item)
#
# print()
# keys = ['name', 'age', 'gender']
# values = ['Timur', 28, 'male']
#
# info = dict(zip(keys, values))
# print(info)
#
# print()
# name = ['Timur', 'Ruslan', 'Rustam']
# age = [28, 21, 19]
#
# for x, y in zip(name, age):
#     print(x, y)
#
# l = [1, 2, 3, 4, 5, 6, 7, 8, 9]
#
# print(*zip(*[iter(l)]*3))


# def ignore_command(command):
#     ignore = ['alias', 'configuration', 'ip', 'sql', 'select', 'update', 'exec', 'del', 'truncate']
#     return any(map(lambda x: x in command, ignore))
#
#
# print(ignore_command('get ip'))
# print(ignore_command('select all'))
# print(ignore_command('delete'))
# print(ignore_command('trancate'))


# https://stackoverflow.com/questions/54009778/what-do-underscores-in-a-number-mean x=100_000_000 ??? int(x) = 100000000
# countries = ['Russia', 'USA', 'UK', 'Germany', 'France', 'India']
# capitals = ['Moscow', 'New York', 'London', 'Berlin', 'Paris', 'Delhi']
# population = [145_934_462, 331_002_651, 80_345_321, 67_886_011, 65_273_511, 1_380_004_385]
#
#
# for (capital, country, people) in zip(capitals, countries, population):
#     print(f'{capital} is the capital of {country}, population equal {people} people.')

# Внутри шара
from functools import reduce as r

# a = map(float, input().split())
# b = map(float, input().split())
# c = map(float, input().split())
#
# print(all(map(lambda x: x <= 2 ** 2, [x ** 2 + y ** 2 + z ** 2 for x, y, z in list(zip(a, b, c))])))


# Корректный IP-адрес

# print(all(list(map(lambda x: x.isdigit() and 0 <= int(x) <= 255, input().split('.')))))

# Интересные числа
# def diter(n):
#     x = []
#     for i in str(n):
#         x.append(n % int(i) == 0)
#     return all(x)
#
#
# a, b = int(input()), int(input())
# q = filter(lambda x: '0' not in str(x), range(a, b + 1))
# z = filter(diter, q)
# print(*z)

# Хороший пароль
# a = input()
# c = all([len(a) >= 7, any(map(lambda x: x.isupper(), a)), any(map(lambda x: x.islower(), a)),
#          any(map(lambda x: x.isdigit(), a))])
# print('YES' if c else 'NO')  # тернарный условный оператор


# Отличники
# s = []
# a = int(input())
# for _ in range(a):  # по кол-ву классов
#     c = int(input())  # № кол-во учеников
#     k = {}
#     for _ in range(c):  # по кол-ву учеников
#         q, w = input().split()
#         k[q] = int(w)
#     s.append(k)
#
#
# q = all(map(lambda x: 5 in x.values(), s))
# print('YES' if q else 'NO')

