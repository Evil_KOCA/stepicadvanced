# n = int(input())
# m = int(input())
# k = int(input())
# x = int(input())
# y = int(input())
# z = int(input())
# t = int(input())
# o = int(input())
#
# a = n + m - x
# b = m + k - y
# c = n + k - z
#
# x1 = n - a - c + t
# x2 = m - a - b + t
# x3 = k - b - c + t
# o1 = x1 + x2 + x3
# o2 = a + b + c - 3 * t
# o0 = o - o1 - o2 - t
# print(o1)
# print(o2)
# print(o0)


#  https://docs.python.org/3/library/stdtypes.html#set
#  https://habr.com/ru/post/516858/

numbers = {1.414, 12.5, 3.1415, 2.718, 9.8, 1.414, 1.1618, 1.324, 2.718, 1.324}

print(min(numbers) + max(numbers))

numbers = {20, 6, 8, 18, 18, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 12, 8, 8, 10, 4, 2, 2, 2, 16, 20}
average = sum(numbers)/len(numbers)


# Не стоит забывать, что множества – неупорядоченные коллекции, поэтому полагаться на порядок вывода элементов не
# стоит. Если нужно гарантировать порядок вывода элементов (по возрастанию / убыванию), то необходимо воспользоваться
# встроенной функцией sorted(). Обратите внимание на то, что функция sorted() возвращает отсортированный список,
# а не множество. Не путайте встроенную функцию sorted() и списочный метод sort(). Множества не содержат метода sort().
numbers = {0, 1, 1, 2, 3, 3, 3, 5, 6, 7, 7}

sorted_numbers = sorted(numbers)
print(*sorted_numbers, sep='\n')


# Встроенная функция sorted() имеет опциональный параметр reverse. Если установить этот параметр в значение True, произойдет сортировка по убыванию.

numbers = {0, 1, 1, 2, 3, 3, 3, 5, 6, 7, 7}

sortnumbers = sorted(numbers, reverse=True)
print(*sortnumbers, sep='\n')

myset1 = set([1, 2, 3, 4, 5])
myset2 = set('12345')
print(myset1,myset2)


numbers = {9089, -67, -32, 1, 78, 23, -65, 99, 9089, 34, -32, 0, -67, 1, 11, 111, 111, 1, 23}
n = 0
for i in numbers:
    n += i**2
print(n)

fruits = {'apple', 'banana', 'cherry', 'avocado', 'pineapple', 'apricot', 'banana', 'avocado', 'grapefruit'}
sortedfruits = sorted(fruits, reverse=True)
print(*sortedfruits, sep='\n')

print(len(set(input())))
a = input()
if len(a) == len(set(a)):
    print('YES')
elif len(a) != len(set(a)):
    print('NO')


a=input()
b=input()
pr = set(range(10))
if set(a) == set(b)==pr:
    print('YES')
else:
    print('NO')

#
a = [set(i) for i in input().split()]

if a[0] == a[1] == a[2]:
    print('YES')
else:
    print('NO')

