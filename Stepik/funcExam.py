# def func(a, b, c, d):
#     return a+b+c+d
#
# print(func(a=1, b=2, c=3, d=4))
# def display(**kwargs):
#     for i in kwargs:
#         print(i, end=' ')
#
# display(emp='Kelly', salary=9000)
#
# num = int('1000001', 2)
# print(num)


# def generate_letter(mail, name, date, time, place, teacher='Тимур Гуев', number=17):
#     print(f'To: {mail}')
#     print(f'Приветствую, {name}!')
#     print(f'Вам назначен экзамен, который пройдет {date}, в {time}.')
#     print(f'По адресу: {place}.')
#     print(f'Экзамен будет проводить {teacher} в кабинете {number}.')
#     print('Желаем удачи на экзамене!')

# Письмо для экзамена
# def generate_letter(mail, name, date, time, place, teacher='Тимур Гуев', number=17):
#     a = f'To: {mail} \nПриветствую, {name}!\nВам назначен экзамен, который пройдет {date}, в {time}.\nПо адресу: {place}.\nЭкзамен будет проводить {teacher} в кабинете {number}.\nЖелаем удачи на экзамене!'
#     return a
#
#
# print(generate_letter('lara@yandex.ru', 'Лариса', '10 декабря', '12:00', 'Часова 23, корпус 2'))
# print()
# print(generate_letter('lara@yandex.ru', 'Лариса', '10 декабря', '12:00',
#                       'Часова 23, корпус 2', 'Василь Ярошевич', 23))


#
# print((lambda x: (x + 3) * 5 / 2)(3))
#
# result = list(filter(str.swapcase, ['a', '1', '', 'b', '2']))
#
# print(result)
#
# files = ['duwwouy440.py', 'crocst0sse.cs', 'j9t7ga2s6x.java', 'jk4nnes4tp.py', '2spc9uqzhu.doc',
#          'qi0ujxe0c7.png', 'z5x7l1j1d8.jpg', 'i5wtdxh366.geo', 'h53s2m2p73.py', 'ojty11f02d.sx',
#          'jyjuwlvwb3.st', 'gv4940lf8m.txt', 'op38fy9m9x.docx', 'o02ltr8vbp.xlsx', 'la97gc4js4.html',
#          'lcihrp8c6l.py', 'z66y7dgfo1.py', 'ckoks0849e.csv']
#
# result = list(filter(lambda s: s.endswith('.py'), files))
#
# print(len(result))
#
# print(list(filter(None, ['', 1, 7, 'beegeek', None, False, 0])))
#
# from functools import reduce
#
# numbers = [1, 2, 3]
# result = reduce(lambda x, y: x + y, numbers)
# print(result)
#
# from functools import reduce
#
# numbers = [1, 2, 3]
# result = reduce(lambda a, b: a * b, numbers, 10)
# print(result)
#
# from functools import reduce
#
# words = ['beegeek', 'stepik', 'python', 'iq-option']
# result = reduce(lambda a, b: a if len(a) > len(b) else b, words)
# print(result)
#
# from functools import reduce
#
# result = reduce(lambda s, x: s + str(x), [1, 2, 3, 4, 5], '+')
# print(result)
#
#
# from functools import reduce
# import operator
#
# def flatten(data):
#     return reduce(operator.concat, data, [])
#
# result = flatten([[1, 2], [3, 4], [], [5]])
#
# print(result)


# def concat(*args,sep = ' '):
#     a = f'{sep.join(map(str, args))}'
#     return a
#
# print(concat('hello', 'python', 'and', 'stepik'))
# print(concat('hello', 'python', 'and', 'stepik', sep='*'))
# print(concat('hello', 'python', sep='()()()'))
# print(concat('hello', sep='()'))
# print(concat(1, 2, 3, 4, 5, 6, 7, 8, 9, sep='$$'))


# from functools import reduce
#
#
# # def product_of_odds(data):   # data - список целых чисел
# #     result = 1
# #     for i in data:
# #         if i % 2 == 1:
# #             result *= i
# #     return result
#
#
# def product_of_odds(data):
#     x = reduce(lambda a, b: a * b, filter(lambda x: x % 2 == 1, data), 1)
#     return x
#
# print(product_of_odds([1, 2, 3 ,4 ,5 ,6,7,8,9]))

# words = 'the world is mine take a look what you have started'.split()
#
# print(*map(lambda x: '"' + x + '"', words))


# numbers = [18, 191, 9009, 5665, 78, 77, 45, 23, 19991, 908, 8976, 6565, 5665, 10, 1000, 908, 909, 232, 45654, 786]
# print(*filter(lambda x: True if str(x) != str(x)[::-1] else False, numbers))


# numbers = [(10, -2, 3, 4), (-13, 56), (1, 9, 2), (-1, -9, -45, 32), (-1, 5, 1), (17, 0, 1), (0, 1), (3,), (39, 12), (11, -23), (10, -100, 21, 32), (3, -8), (1, 1)]
#
# sorted_numbers = sorted(numbers, key=lambda x: sum(x) / len(x), reverse= True)
#
# print(sorted_numbers)


# def mul7(x):
#     return x * 7
#
#
# def add2(x, y):
#     return x + y
#
#
# def add3(x, y, z):
#     return x + y + z
#
# def call(func, *args):
#     return func(*args)
#
#
# print(call(mul7, 10))
# print(call(add2, 2, 7))
# print(call(add3, 10, 30, 40))
# print(call(bool, 0))


# def add3(x):
#     return x + 3
#
#
# def mul7(x):
#     return x * 7
#
# def compose(f,g):
#     def compose2(x):
#         return f(g(x))
#     return compose2
#
#
# print(compose(mul7, add3)(1))
# print(compose(add3, mul7)(2))
# print(compose(mul7, str)(3))
# print(compose(str, mul7)(5))

# import operator
# def arithmetic_operation(symbol):
#     k = {'+': operator.add, '-': operator.sub, '*': operator.mul, '/': operator.truediv}
#     return k[symbol]
#
#
# add = arithmetic_operation('+')
# div = arithmetic_operation('/')
# print(add(10, 20))
# print(div(20, 5))

# В одну строку

# print(*sorted(input().split(), key=lambda x: x.lower()))


#  Гематрия слова
# n = int(input())
# a = [input() for _ in range(n)]
# a = sorted(a)
#
#
# def gema(word):
#     return sum(map(lambda x: ord(x.upper()) - ord('A'), word))
#
# q = sorted(a, key=gema)
# for i in q:
#     print(i)


# Сортировка IP-адресов
# n = int(input())
# a = [input() for _ in range(n)]

# def signature(ip):
#     mnoj = list(map(lambda x: 256 ** x, range(0, 4)[::-1]))
#     iparr = list(map(int, ip.split('.')))
#     return sum([x * y for x,y in zip(mnoj,iparr)])
#
# a = sorted(a,key=signature)
# for i in a:
#     print(i)

# Pretty print

# def concat(*args,sep = ' '):
#     a = f'{sep.join(map(str, args))}'
#     return a


def pretty_print(args, side='-', delimiter='|'):
    a = ''
    for i in args:
        a += f'{delimiter} {i} '
    bord = len(a) - 1
    c = f' {side * bord} \n{a}{delimiter}\n {side * bord} '
    print(c)


pretty_print([1, 2, 10, 23, 123, 3000])
pretty_print(['abc', 'def', 'ghi', '12345'])
pretty_print(['abc', 'def', 'ghi'], side='*')
pretty_print(['abc', 'def', 'ghi'], delimiter='#')
pretty_print(['abc', 'def', 'ghi'], side='*', delimiter='#')
