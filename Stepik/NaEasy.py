# На Easy

# a = int(input())
# b = int(input())
# print(a + b)
# print(a - b)
# print(a * b)
# print(a / b)
# print(a // b)
# print(a % b)
# print((a ** 10 + b ** 10) ** 0.5)

# Индекс массы тела
# m = float(input())
# l = float(input())
# ind = m / (l * l)
# if 18.5 <= ind <= 25:
#     print('Оптимальная масса')
# elif ind > 25:
#     print('Избыточная масса')
# elif ind < 18.5:
#     print('Недостаточная масса')

# Стоимость строки
# a = input()
# m = 60 * len(a)
# print(m // 100, 'р.', m % 100, 'коп.')

# Количество слов
# print(len(input().split()))

# Зодиак
# z = ['Обезьяна', 'Петух', 'Собака', 'Свинья', 'Крыса', 'Бык', 'Тигр', 'Заяц', 'Дракон', 'Змея', 'Лошадь', 'Овца']
# a = []
# for i in range(13):
#     a = a + [i]
# zo = {key: value for key, value in zip(a, z)}
# n = int(input())
# n = n % 12
# print(zo[n])


# Переворот числа

# num = int(input())
#
#
# def around(numF):
#     ar = str(numF)
#     ar1 = ar[-1:-6:-1]
#     if len(ar) == 6:
#         ar = ar[0] + ar1
#         return ar
#     elif len(ar) == 5:
#         return ar1
#
#
# def noZero(strF):
#     j = 0
#     for i in strF:
#         if i == '0':
#             j += 1
#         elif i != '0':
#             break
#     k = len(strF)
#     ind = strF[j: k]
#     return ind
#
#
# print(int(noZero(around(num))))


# Standard American Convention


# ss = input()
# def massivS(s):
#     ite = len(s) // 3
#     ite1 = len(s) % 3
#     sshort = s[ite1:len(s)]
#     q = []
#     for i in range(0, ite):
#         q = q + [sshort[0 + 3 * i: 3 + 3 * i]]
#     if ite1 != 0:
#         q = [s[0: ite1]] + q
#     return q
#
# def printS(q):
#     for i in range(len(q)-1):
#         print(q[i], end=',')
#     print(q[-1])
#
# qq = massivS(ss)
# printS(qq)
# Альтернативное решение в одну строку
# print('{:,}'.format(input()))


# Задача Иосифа Флавия 🌶️🌶️
# https://ru.wikipedia.org/wiki/%D0%97%D0%B0%D0%B4%D0%B0%D1%87%D0%B0_%D0%98%D0%BE%D1%81%D0%B8%D1%84%D0%B0_%D0%A4%D0%BB%D0%B0%D0%B2%D0%B8%D1%8F
# https://www.geogebra.org/m/ExvvrBbR

# n = int(input())
# m = int(input())
#
# res = 0
# for i in range(1, n + 1):
#     res = (res + m) % i
# print(res + 1)


# Coordinates

# n = int(input())
# q = []
# for i in range(n):
#     q = q + [(input().split())]
# a = []
# for i in q:
#     i[0] = int(i[0])
#     i[1] = int(i[1])
# z = {'Первая четверть': 0, 'Вторая четверть': 0, 'Третья четверть': 0, 'Четвертая четверть': 0}
# for i in q:
#     if i[0] > 0 and i[1] > 0:
#         z['Первая четверть'] += 1
#     elif i[0] > 0 and i[1] < 0:
#         z['Четвертая четверть'] += 1
#     elif i[0] < 0 and i[1] < 0:
#         z['Третья четверть'] += 1
#     elif i[0] < 0 and i[1] > 0:
#         z['Вторая четверть'] += 1
# for key, value in z.items():
#     print(key, end=': ')
#     print(value)

# Bigger
# q = input().split()
# z = []
# for i in q:
#     z += [int(i)]
# q = max(z)
# j = 0
# for i in z:
#     if i > q:
#         j += 1
#     q = i
# print(j)

# Forward, back and vice versa
# q = input().split()
# z = []
# for i in q:
#     z += [int(i)]
# for i in range(0, len(z)-1, 2):
#     q = z[i]
#     z[i] = z[i+1]
#     z[i+1] = q
# print(*z)


# Evolution Shift
# z = input().split()
# z = [z[-1]] + z[-len(z):-1:1]
# print(*z)

# Different elements
# qq = input().split()
# q = {}
# for i in qq:
#     q[i] = 0
# print(len(q))


# Multiply
# n = int(input())
# q = []
# for i in range(n):
#     q += [int(input())]
# k = int(input())
# for i in range(len(q)):
#     for j in range(i+1, len(q)):
#         if q[i]*q[j] == k:
#             n = 1
#             break
#     if n == 1:
#         break
# if n != 1 or len(q) == 1:
#     print("НЕТ")
# else:
#     print("ДА")


# Rock, Paper, Scissors
# T = input()
# R = input()
# if T == R:
#     print('ничья')
# elif T+R in ['ножницыкамень', 'каменьбумага', 'бумаганожницы']:
#     print('Руслан')
# else:
#     print('Тимур')

# Rock, Paper, Scissors, Lizard, Spok
# t = input()
# r = input()
# graf = {'ножницы': ['бумага', 'ящерица'], 'бумага': ['камень', 'Спок'], 'камень': ['ножницы', 'ящерица'],
#         'ящерица': ['Спок', 'бумага'], 'Спок': ['ножницы', 'камень']}
# if r in graf[t]:
#         print('Тимур')
# elif t in graf[r]:
#         print("Руслан")
# else:
#         print('ничья')
#
# # Head and Tail
# a = input().split('О')
# print(len(max(a)))

# Virus Anton
# n = int(input())
# sam = []
# for i in range(n):
#         sam = sam + [input()]
# def ant(nam, vir):
#     n = 0
#     for i in vir:
#         for j in nam:
#             if i == j:
#                 q = (nam.find(i))
#                 nam = nam[q + 1:len(nam)]
#                 n += 1
#                 break
#     if n == len(vir):
#         return True
#     else:
#         return False
#
#
# vi = 'anton'
# for i in sam:
#         if (ant(i, vi)):
#                 print(sam.index(i) + 1, end=' ')


# Роскомнадзор запретил букву а 🌶️🌶️
# word = 'роскомнадзор'
word = input()
phr = 'запретил букву'
alp = 'абвгдежзийклмнопрстуфхцчшщъыьэюя'
q = word + ' ' + phr + ' '
word = q
for i in alp:
    if i in word:
        word = word + i
        print(word)
        word = word.replace(i, '').replace('  ', ' ').lstrip()


