numbers = (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10)

for i in range(len(numbers)):
    print(numbers[i], end=' ')

numbers = (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10)

print()
for num in numbers:
    print(num, end=' ')

# Можно также использовать операцию распаковки кортежа.
numbers = (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
languages = ('Python', 'C++', 'Java')
print()

print(*numbers)
print(*languages, sep='\n')

print((1, 8) == (1, 8))
print((1, 8) != (1, 10))
print((1, 9) < (1, 2))
print((2, 5) < (6,))
print(('a', 'bc') > ('a', 'de'))

# Обратите внимание, что функция sorted() возвращает список, но с помощью функции tuple() мы приводим результат
# сортировки к кортежу.
not_sorted_tuple = (34, 1, 8, 67, 5, 9, 0, 23)
print(not_sorted_tuple)

sorted_tuple = tuple(sorted(not_sorted_tuple))
print(sorted_tuple)

# Часто на практике нам приходится преобразовывать кортежи в списки и в строки. Для этого используются функции и
# методы str(), list(), tuple(), join().

tuple1 = (1, 2, 3, 4, 5)
list1 = list(tuple1)
print(list1)

list1 = [1, 17.8, 'Python']
tuple1 = tuple(list1)
print(tuple1)

# Кортеж можно преобразовать в строку с помощью строкового метода join().
notes = ('Do', 'Re', 'Mi', 'Fa', 'Sol', 'La', 'Si')
string1 = ''.join(notes)
string2 = '.'.join(notes)

print(string1)
print(string2)

# Строку можно преобразовать в кортеж с помощью функции tuple().

letters = 'abcdefghijkl'
tpl = tuple(letters)
print(tpl)

a = [1, 2, 3]
b = (1, 2, 3)

a = str(a)
b = str(b)
c = a + b
print(c)

poets = [('Есенин', 13), ('Тургенев', 14), ('Маяковский', 28), ('Лермонтов', 20), ('Фет', 15)]

for i in range(len(poets)):
    for j in range(i + 1, len(poets)):
        if poets[i][1] > poets[j][1]:
            poets[i], poets[j] = poets[j], poets[i]

print(poets[0])
print(poets[-1])

numbers = (2, 3, 5, 7, -11, 13, 17, 19, 23, 29, 31, -6, 41, 43, 47, 53, 59, 61, -96, 71, 1000, -1)
a = 1
for i in numbers:
    a = a * i
print(a)

data = 'Python для продвинутых!'
a = tuple(data)
print(a)

poet_data = ('Пушкин', 1799, 'Санкт-Петербург')
a = list(poet_data)
a[-1] = 'Москва'
poet_data = tuple(a)
print(poet_data)

numbers = ((10, 10, 10, 12), (30, 45, 56, 45), (81, 80, 39, 32), (1, 2, 3, 4), (90, 10))
a = []
for i in numbers:
    a = a + [sum(i) / len(i)]
print(a)

# print()
# print()
# print()
# a = int(input())
# b = int(input())
# c = int(input())
#
# d = (4*a*c - b**2) / (4*a)
# x1 = -b / (2*a)
# q = (x1, d)
# print(q)


# n = int(input())
# a = [(input().split()) for i in range(n)]
#
# b = [i for i in a if int(i[1]) >= 4]
# for i in a:
#     print(*i)
# print()
# for i in b:
#     print(*i)

# Распаковка кортежей Обратная операция, смысл которой в том, чтобы присвоить значения элементов кортежа отдельным
# переменным называется распаковкой кортежа. Упаковкой кортежа называют присваивание его какой-либо переменной.

tuple1 = (1, 2, 3)
tuple2 = ('b',)
tuple3 = ('red', 'green', 'blue', 'cyan')

print(type(tuple1))
print(type(tuple2))
print(type(tuple3))

colors = ('red', 'green', 'blue', 'cyan')

(a, b, c, d) = colors

print(a)
print(b)
print(c)
print(d)

colors = ('red', 'green', 'blue')
a, b, _ = colors

print(a)
print(b)

a, b, c = 3, 2, 1
b, a, c = c, a, b

print(b, c, a)

#  * при распаковке кортежей
a, b, *tail = 1, 2, 3, 4, 5, 6


*names, surname = ('Стефани', 'Джоанн', 'Анджелина', 'Джерманотта')

print(names)
print(surname)


singer = ('Freddie', 'Bohemian Rhapsody', 'Killer Queen', 'Love of my life', 'Mercury')

name, *songs, surname = singer

print(name)
print(songs)
print(surname)

#  Примечание 1. Если вы хотите распаковать единственное значение в кортеже, после имени переменной должна идти запятая.
a = 1,      # не распаковка, а просто присвоение
b, = 1,     # распаковка

print(a)
print(b)


# Примечание 2. Распаковывать можно не только кортеж, правая сторона может быть любой последовательностью (кортеж,
# строка или список).
info = ['timur', 'beegeek.org']
user, domain = info    # распаковка списка

print(user)
print(domain)

a, b, c, d = 'math'    # распаковка строки

print(a)
print(b)
print(c)
print(d)


# Примечание 3. Помимо метода split() строковый тип данных содержит метод partition(). Метод partition() принимает на
# вход один аргумент sep, разделяет строку при первом появлении sep и возвращает кортеж, состоящий из трех элементов:
# часть перед разделителем, сам разделитель и часть после разделителя. Если разделитель не найден, то кортеж содержит
# саму строку, за которой следуют две пустые строки.


s1 = 'abc-de'.partition('-')
s2 = 'abc-de'.partition('.')
s3 = 'abc-de-fgh'.partition('-')
print(s1)
print(s2)
print(s3)


# Примечание 4. С использованием кортежей многие алгоритмы приобретают достаточно краткую форму. Например,
# вычисление чисел Фибоначчи может выглядеть следующим образом:

# n = int(input())
# f1, f2 = 1, 1
# for i in range(n):
#     print(f1)
#     f1, f2 = f2, f1 + f2


# Примечание 5. Замечательная серия статей о коллекциях (list, tuple, str, set, dict) в Python.
# https://habr.com/ru/post/319164/


def trib(n):
    q = (1, 1, 1)
    if n == 1:
        print(1)
    elif n == 2:
        print(*(1, 1))
    elif n >= 3:
        for i in range(3, n):
            q = q + (q[-1] + q[-2] + q[-3],)
        print(*q)
        return q







k = 10
q = trib(k)



