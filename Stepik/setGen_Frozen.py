# digits = {int(c) for c in input()}
squares = {i ** 2 for i in range(10)}
cubes = {i ** 3 for i in range(10, 21)}
chars = {c for c in 'abcdefg'}
digits = {int(d) for d in 'abcd12ef78ghj90' if d.isdigit()}


items = [10, '30', 30, 10, '56', 34, '12', 90, 89, 34, 45, '67', 12, 10, 90, 23, '45', 56, '56', 1, 5, '6', 5]
a = 5

myset = {int(i) for i in items}
print(*sorted(myset))

words = ['Plum', 'Grapefruit', 'apple', 'orange', 'pomegranate', 'Cranberry', 'lime', 'Lemon', 'grapes', 'persimmon', 'tangerine', 'Watermelon', 'currant', 'Almond']

myset = {i[0].lower() for i in words}
a = sorted(myset)
print(*a)


sentence = '''My very photogenic mother died in a freak accident (picnic, lightning) when I was three, and, save for a pocket of warmth in the darkest past, nothing of her subsists within the hollows and dells of memory, over which, if you can still stand my style (I am writing under observation), the sun of my infancy had set: surely, you all know those redolent remnants of day suspended, with the midges, about some hedge in bloom or suddenly entered and traversed by the rambler, at the bottom of a hill, in the summer dusk; a furry warmth, golden midges.'''
a = {i.strip('(),.:;') for i in sentence.lower().split() if len(i.strip('(),.:;')) < 4}
b = sorted(a)
print(*b)

files = ['python.png', 'qwerty.py', 'Python.PNg', 'apple.pnG', 'zebra.PNG',  'solution.Py', 'stepik.org', 'kotlin.ko', 'github.git', 'ZeBrA.PnG']
a = {i.lower() for i in files if i[-1:-5:-1][::-1].lower() == '.png'}
b = sorted(a)
print(*b)


# Frozenset   Результатом операций над замороженными множествами будут тоже замороженные множества.
myset1 = frozenset({1, 2, 3})                         # на основе множества
myset2 = frozenset([1, 1, 2, 3, 4, 4, 4, 5, 6, 6])    # на основе списка
myset3 = frozenset('aabcccddee')                      # на основе строки

print(myset1)
print(myset2)
print(myset3)

print(

)
myset1 = frozenset('hello')
myset2 = frozenset('world')

print(myset1 | myset2)
print(myset1 & myset2)
print(myset1 ^ myset2)
print()


sentence = 'The cat in the hat had two sidekicks, thing one and thing two.'

words = sentence.lower().replace('.', '').replace(',', '').split()

vowels = ['a', 'e', 'i', 'o', 'u']

consonants = {frozenset({letter for letter in word if letter not in vowels}) for word in words}

print(*consonants, sep='\n')

myset1 = set('qwerty')
myset2 = frozenset('qwerty')

print(myset1 == myset2)