# my_dict = {'C1': [10, 20, 30, 7, 6, 23, 90], 'C2': [20, 30, 40, 1, 2, 3, 90, 12], 'C3': [12, 34, 20, 21], 'C4': [22, 54, 209, 21, 7], 'C5': [2, 4, 29, 21, 19], 'C6': [4, 6, 7, 10, 55], 'C7': [4, 8, 12, 23, 42], 'C8': [3, 14, 15, 26, 48], 'C9': [2, 7, 18, 28, 18, 28]}
# for key, value in my_dict.items():
#     a = []
#     for i in value:
#         if i <= 20:
#             a.append(i)
#     my_dict[key] = a
# print(my_dict)

# emails = {'nosu.edu': ['timyr', 'joseph', 'svetlana.gaeva', 'larisa.mamuk'],
#           'gmail.com': ['ruslan.chaika', 'rustam.mini', 'stepik-best'],
#           'msu.edu': ['apple.fruit', 'beegeek', 'beegeek.school'],
#           'yandex.ru': ['surface', 'google'],
#           'hse.edu': ['tomas-henders', 'cream.soda', 'zivert'],
#           'mail.ru': ['angel.down', 'joanne', 'the.fame.moster']}
#
# s = []
# for key, value in emails.items():
#     for i in value:
#         s.append(i + '@' + key)
# b = sorted(s)
# for i in b:
#     print(i)

# Минутка генетики
# dnk = ['A', 'C', 'G', 'T']
# rnk = ['U', 'G', 'C', 'A']
# tr = {key: value for key, value in zip(dnk, rnk)}
# a = input()
# for i in a:
#     print(tr[i],end='')

# # Порядковый номер
# a = input()
# d = {}
# for i in a.split():
#     d[i] = d.get(i,0) + 1
#     print(d[i],end=' ')

# Scrabble game
# d = {
#     1: "AEILNORSTU",
#     2: "DG",
#     3: "BCMP",
#     4: "FHVWY",
#     5: "K",
#     8: "JX",
#     10: "QZ"
# }
# a = input()
# n = 0
# for i in a:
#     for key, value in d.items():
#         if i in value:
#             n = n + key
# print(n)

# # Строка запроса
# def build_query_string(po):
#     a = []
#     for i in po:
#         a += [i]
#     b = sorted(a)
#     s = ''
#     for i in b:
#         s = s + i + '=' + str(po[i]) + '&'
#     s = s.strip('&')
#     return s
#
#
# print(build_query_string({'sport': 'hockey', 'game': 2, 'time': 17}))
# print(build_query_string({'name': 'timur', 'age': 28}))
# print(build_query_string({'sport': 'hockey', 'game': 2, 'time': 17}))


# Слияние словарей 🌶️
# def merge(po):
#     d = {}
#     s = set([])
#     for i in po:
#         for j in i:
#             s.add(j)
#     for i in s:
#         d[i] = set()
#     for i in po:
#         for key, value in i.items():
#             d[key] = d[key] | {value}
#     return d
#
#
# p = [{'a': 1, 'b': 2}, {'b': 10, 'c': 100}, {'a': 1, 'b': 17, 'c': 50}, {'a': 5, 'd': 777}]
# result = merge(p)
# print(result)


# # Опасный вирус 😈
# d = {}
# n = int(input())
# for i in range(n):
#     a, *tail = input().split()
#     d[a] = tail
#
#
# e = []
# m = int(input())
# for i in range(m):
#     a, b = input().split()
#     e += [{b:a}]
#
# s = {'write': 'W', 'read': 'R','execute': 'X'}
#
# for i in e:
#     for q, p in i.items():
#         if s[p] in d[q]:
#             print('OK')
#         elif s[p] not in d[q]:
#             print('Access denied')
#

# Покупки в интернет-магазине 🌶️


def merge(po):
    d = {}
    s = set([])
    for i in po:
        for j in i:
            s.add(j)
    for i in s:
        d[i] = 0
    for i in po:
        for key, value in i.items():
            d[key] = d[key] + value
    return d


s = []
d = {}
m = int(input())
for _ in range(m):
    a, b, c = input().split()
    s += [{a: {b: int(c)}}]
for i in s:
    for key, value in i.items():
        for q, p in value.items():
            d[key] = d.get(key,[]) + [{q:p}]
for key in d:
    d[key] = merge(d[key])
k = sorted(d)
for i in k:
    print(i + ':')
    a = sorted(d[i])
    for j in a:
        print(j, d[i][j])



