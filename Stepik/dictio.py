# languages = [('Python', 'Гвидо ван Россум'),
#              ('C#', 'Андерс Хейлсберг'),
#              ('Java', 'Джеймс Гослинг'),
#              ('C++', 'Бьёрн Страуструп')]
# print(languages)
# print(type(languages))
#
# fruits = {'Apple': 70, 'Grape': 100, 'Banana': 80}
# capitals = {'Россия': 'Москва', 'Франция': 'Париж'}
#
# print(len(fruits))
# print(len(capitals))
#
# capitals = {'Россия': 'Москва', 'Франция': 'Париж', 'Чехия': 'Прага'}
#
# if 'Франция' in capitals:
#     print('Столица Франции - это', capitals['Франция'])
#
# my_dict = {10: 'Россия', 20: 'США', 30: 'Франция'}
# print('Сумма всех ключей словаря =', sum(my_dict))
#
# capitals = {'Россия': 'Москва', 'Франция': 'Париж', 'Чехия': 'Прага'}
# months = {1: 'Январь', 2: 'Февраль', 3: 'Март'}
#
# print('Минимальный ключ =', min(capitals))
# print('Максимальный ключ =', max(months))
# print()
#
# months1 = {1: 'Январь', 2: 'Февраль'}
# months2 = {1: 'Январь', 2: 'Февраль', 3: 'Март'}
# months3 = {3: 'Март', 1: 'Январь', 2: 'Февраль'}
#
# print(months1 == months2)
# print(months2 == months3)
# print(months1 != months3)
#
# capitals = {'Россия': 'Москва', 'Франция': 'Париж', 'Чехия': 'Прага'}
#
# for key in capitals:
#     print(key)
#
# print()
# print()
# capitals = {'Россия': 'Москва', 'Франция': 'Париж', 'Чехия': 'Прага'}
#
# for key in capitals:
#     print(capitals[key])
#
# print()
# print()
# capitals = {'Россия': 'Москва', 'Франция': 'Париж', 'Чехия': 'Прага'}
#
# for key in capitals:
#     print('Столица', key, '- это', capitals[key])
#
# capitals = {'Россия': 'Москва', 'Франция': 'Париж', 'Чехия': 'Прага'}
#
# for key in capitals.keys():  # итерируем по списку ['Россия', 'Франция', 'Чехия']
#     print(key)
#
# capitals = {'Россия': 'Москва', 'Франция': 'Париж', 'Чехия': 'Прага'}
#
# for value in capitals.values():  # итерируем по списку ['Москва', 'Париж', 'Прага']
#     print(value)
#
# capitals = {'Россия': 'Москва', 'Франция': 'Париж', 'Чехия': 'Прага'}
#
# for item in capitals.items():
#     print(item)
#
# capitals = {'Россия': 'Москва', 'Франция': 'Париж', 'Чехия': 'Прага'}
#
# for key, value in capitals.items():
#     print(key, '-', value)
#
# capitals = {'Россия': 'Москва', 'Франция': 'Париж', 'Чехия': 'Прага'}
# print(*capitals, sep='\n')
#
# capitals = {'Россия': 'Москва', 'Англия': 'Лондон', 'Чехия': 'Прага', 'Бразилия': 'Бразилиа'}
#
# print()
# print()
# print()
# for key, value in sorted(capitals.items(), key=lambda x: x[1]):
#     print(value)

#
# users = [{'name': 'Todd', 'phone': '551-1414', 'email': 'todd@gmail.com'},
#          {'name': 'Helga', 'phone': '555-1618', 'email': 'helga@mail.net'},
#          {'name': 'Olivia', 'phone': '449-3141', 'email': ''},
#          {'name': 'LJ', 'phone': '555-2718', 'email': 'lj@gmail.net'},
#          {'name': 'Ruslan', 'phone': '422-145-9098', 'email': 'rus-lan.cha@yandex.ru'},
#          {'name': 'John', 'phone': '233-421-32', 'email': ''},
#          {'name': 'Lara', 'phone': '+7998-676-2532', 'email': 'g.lara89@gmail.com'},
#          {'name': 'Alina', 'phone': '+7948-799-2434', 'email': 'ali.ch.b@gmail.com'},
#          {'name': 'Robert', 'phone': '420-2011', 'email': ''},
#          {'name': 'Riyad', 'phone': '128-8890-128', 'email': 'r.mahrez@mail.net'},
#          {'name': 'Khabib', 'phone': '+7995-600-9080', 'email': 'kh.nurmag@gmail.com'},
#          {'name': 'Olga', 'phone': '6449-314-1213', 'email': ''},
#          {'name': 'Roman', 'phone': '+7459-145-8059', 'email': 'roma988@mail.ru'},
#          {'name': 'Maria', 'phone': '12-129-3148', 'email': 'm.sharapova@gmail.com'},
#          {'name': 'Fedor', 'phone': '+7445-341-0545', 'email': ''},
#          {'name': 'Tim', 'phone': '242-449-3141', 'email': 'timm.ggg@yandex.ru'}]
# a = []
# for i in users:
#     for key, value in i.items():
#         if key == 'phone' and value[-1] == '8':
#             a += [i['name']]
# a = sorted(a)
# print(*a)


#
#
# users = [{'name': 'Todd', 'phone': '551-1414', 'email': 'todd@gmail.com'},
#          {'name': 'Helga', 'phone': '555-1618'},
#          {'name': 'Olivia', 'phone': '449-3141', 'email': ''},
#          {'name': 'LJ', 'phone': '555-2718', 'email': 'lj@gmail.net'},
#          {'name': 'Ruslan', 'phone': '422-145-9098', 'email': 'rus-lan.cha@yandex.ru'},
#          {'name': 'John', 'phone': '233-421-32', 'email': ''},
#          {'name': 'Lara', 'phone': '+7998-676-2532', 'email': 'g.lara89@gmail.com'},
#          {'name': 'Alina', 'phone': '+7948-799-2434'},
#          {'name': 'Robert', 'phone': '420-2011', 'email': ''},
#          {'name': 'Riyad', 'phone': '128-8890-128', 'email': 'r.mahrez@mail.net'},
#          {'name': 'Khabib', 'phone': '+7995-600-9080', 'email': 'kh.nurmag@gmail.com'},
#          {'name': 'Olga', 'phone': '6449-314-1213', 'email': ''},
#          {'name': 'Roman', 'phone': '+7459-145-8059'},
#          {'name': 'Maria', 'phone': '12-129-3148', 'email': 'm.sharapova@gmail.com'},
#          {'name': 'Fedor', 'phone': '+7445-341-0545', 'email': ''},
#          {'name': 'Tim', 'phone': '242-449-3141', 'email': 'timm.ggg@yandex.ru'}]
#
# a = []
# for i in users:
#     for key, value in i.items():
#         if key == 'email' and value == '':
#             a += [i['name']]
#     if 'email' in i.keys():
#         continue
#     else:
#         a += [i['name']]
# a = sorted(a)
# print(*a)
#
# a = []
# for i in range(10):
#     a += [str(i)]
# b = ['zero','one','two','three','four','five','six','seven','eight','nine']
# k = {key:value for key,value in zip(a,b)}
#
# pa = input()
# for i in pa:
#     print(k[i], end=' ')

# b = {'CS101': (3004, 'Хайнс', '8:00'), 'CS102': (4501, 'Альварадо', '9:00'), 'CS103': (6755, 'Рич', '10:00'), 'NT110': (1244, 'Берк', '11:00'), 'CM241': (1411, 'Ли', '13:00')}
#
#
# n = input()
# print(n, end=': ')
# x = '{0}, {1}, {2}'.format(b[n][0],b[n][1],b[n][2])
# print(x)


# d = {
#     "1": ".,?!:",
#     "2": "ABC",
#     "3": "DEF",
#     "4": "GHI",
#     "5": "JKL",
#     "6": "MNO",
#     "7": "PQRS",
#     "8": "TUV",
#     "9": "WXYZ",
#     "0": " "
# }
#
# a = input().upper()
#
# for i in a:
#     for key, value in d.items():
#         if i in value:
#             print(key * (value.index(i) + 1), end='')
#

# Morse
# letters = [c for c in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789']
# morse = ['.-', '-...', '-.-.', '-..', '.', '..-.', '--.', '....', '..', '.---', '-.-', '.-..', '--', '-.', '---', '.--.', '--.-', '.-.', '...', '-', '..-', '...-', '.--', '-..-', '-.--', '--..', '-----', '.----', '..---', '...--', '....-', '.....', '-....', '--...', '---..', '----.']
#
# d = {key:value for key, value in zip(letters,morse)}
#
#
# a = input().upper()
# for i in a:
#     if i in d:
#         print(d[i], end=' ')


# Добавление и изменение элементов в словаре
# Удаление элементов из словаря
# Методы get(), update()
# Методы pop(), popitem()
# Методы clear(), copy()
# Метод setdefault()

# # Метод get()
# info = {'name': 'Bob',
#         'age': 25,
#         'job': 'Dev'}
#
# item1 = info.get('salary')
# item2 = info.get('salary', 'Информации о зарплате нет')
#
# print(item1)
# print(item2)
#
#
# numbers = [9, 8, 32, 1, 10, 1, 10, 23, 1, 4, 10, 4, 2, 2, 2, 2, 1, 10, 1, 2, 2, 32, 23, 23]
#
# result = {}
# for num in numbers:
#     result[num] = result.get(num, 0) + 1
# print(result)
#
# # Метод update() Метод update() – реализует своеобразную операцию конкатенации для словарей. Он объединяет ключи и
# # значения одного словаря с ключами и значениями другого. При совпадении ключей в итоге сохранится значение словаря,
# # указанного в качестве аргумента метода update().Метод update() – реализует своеобразную операцию конкатенации для
# # словарей. Он объединяет ключи и значения одного словаря с ключами и значениями другого. При совпадении ключей в
# # итоге сохранится значение словаря, указанного в качестве аргумента метода update().
#
# info1 = {'name': 'Bob',
#         'age': 25,
#         'job': 'Dev'}
#
# info2 = {'age': 30,
#         'city': 'New York',
#         'email': 'bob@web.com'}
#
# info1.update(info2)
#
# print(info1)
#
# info1 = {'name': 'Bob',
#         'age': 25,
#         'job': 'Dev'}
#
# info2 = {'age': 30,
#         'city': 'New York',
#         'email': 'bob@web.com'}
#
# info1 |= info2
#
# print(info1)
#
#
# # Метод setdefault()
# # Метод setdefault() позволяет получить значение из словаря по заданному ключу, автоматически добавляя элемент словаря, если он отсутствует.
# #
# # Метод принимает два аргумента:
# #
# #  key: ключ, значение по которому следует получить, если таковое имеется в словаре, либо создать.
# #  default: значение, которое будет использовано при добавлении нового элемента в словарь.
# # В зависимости от значений параметров key и default возможны следующие сценарии работы данного метода.
#
# # Сценарий 1. Если ключ key присутствует в словаре, то метод возвращает значение по заданному ключу (независимо от того, передан параметр default или нет).
# #
# # Приведенный ниже код:
# info = {'name': 'Bob',
#         'age': 25}
#
# name1 = info.setdefault('name')           # параметр default не задан
# name2 = info.setdefault('name', 'Max')    # параметр default задан
#
# print(name1)
# print(name2)
#
# # Сценарий 2. Если ключ key отсутствует в словаре, то метод вставляет переданное значение default по заданному ключу.
# info = {'name': 'Bob',
#         'age': 25}
#
# job = info.setdefault('job', 'Dev')
# print(info)
# print(job)
#
# # Удаление элементов из словаря
# # Оператор del
# info = {'name': 'Sam',
#         'age': 28,
#         'job': 'Teacher',
#         'email': 'timyr-guev@yandex.ru'}
#
# del info['email']    # удаляем элемент имеющий ключ email
# del info['job']      # удаляем элемент имеющий ключ job
#
# # Метод pop()
# info = {'name': 'Sam',
#         'age': 28,
#         'job': 'Teacher',
#         'email': 'timyr-guev@yandex.ru'}
#
# email = info.pop('email')          # удаляем элемент по ключу email, возвращая его значение
# job = info.pop('job')              # удаляем элемент по ключу job, возвращая его значение
#
# print(email)
# print(job)
# print(info)
# surname = info.pop('surname', None)
#
# # Метод popitem()
# info = {'name': 'Bob',
#      'age': 25,
#      'job': 'Dev'}
#
# info['surname'] = 'Sinclar'
#
# item = info.popitem()
#
# print(item)
# print(info)
#
# # Метод clear()
# info = {'name': 'Bob',
#         'age': 25,
#         'job': 'Dev'}
#
# info.clear()
#
# print(info)
# # Метод copy()
# info = {'name': 'Bob',
#         'age': 25,
#         'job': 'Dev'}
#
# info_copy = info.copy()
#
# print(info_copy)
#
# result = {i:i**2 for i in range(1,16)}

#
#
# dict1 = {'a': 100, 'z': 333, 'b': 200, 'c': 300, 'd': 45, 'e': 98, 't': 76, 'q': 34, 'f': 90, 'm': 230}
# dict2 = {'a': 300, 'b': 200, 'd': 400, 't': 777, 'c': 12, 'p': 123, 'w': 111, 'z': 666}
# a = set(dict1) | set(dict2)
# result = dict()
# for i in a:
#     result[i] = dict1.get(i,0) + dict2.get(i,0)

# text = 'footballcyberpunkextraterritorialityconversationalistblockophthalmoscopicinterdependencemamauserfff'
#
# result = {}
# for i in text:
#     result[i] = result.get(i,0) + 1
# print(result)

# s = 'orange strawberry barley gooseberry apple apricot barley currant orange melon pomegranate banana banana orange barley apricot plum grapefruit banana quince strawberry barley grapefruit banana grapes melon strawberry apricot currant currant gooseberry raspberry apricot currant orange lime quince grapefruit barley banana melon pomegranate barley banana orange barley apricot plum banana quince lime grapefruit strawberry gooseberry apple barley apricot currant orange melon pomegranate banana banana orange apricot barley plum banana grapefruit banana quince currant orange melon pomegranate barley plum banana quince barley lime grapefruit pomegranate barley'
# result = {}
# for i in s.split():
#     result[i] = result.get(i,0) + 1
#
# a = max(result.values())
#
# z = [key for key in result if result[key] == a]
# print(min(z))

#
# pets = [('Hatiko', 'Parker', 'Wilson', 50),
#         ('Rusty', 'Josh', 'King', 25),
#         ('Fido', 'John', 'Smith', 28),
#         ('Butch', 'Jake', 'Smirnoff', 18),
#         ('Odi', 'Emma', 'Wright', 18),
#         ('Balto', 'Josh', 'King', 25),
#         ('Barry', 'Josh', 'King', 25),
#         ('Snape', 'Hannah', 'Taylor', 40),
#         ('Horry', 'Martha', 'Robinson', 73),
#         ('Giro', 'Alex', 'Martinez', 65),
#         ('Zooma', 'Simon', 'Nevel', 32),
#         ('Lassie', 'Josh', 'King', 25),
#         ('Chase', 'Martha', 'Robinson', 73),
#         ('Ace', 'Martha', 'Williams', 38),
#         ('Rocky', 'Simon', 'Nevel', 32)]
#
# result = {}
# for i in pets:
#     a, *tail = i
#     t = tuple(tail)
#     if result.get(t) == None:
#         result[t] = [a]
#     else:
#         result[t] += [a]


# Самое редкое слово 🌶️
# a = [i.lower().strip('.,!?:;-') for i in input().split()]
#
# d = {}
# s = set()
# for i in a:
#     s.add(i)
# for i in s:
#     d[i] = a.count(i)
# n = min(d.values())
# m = [key for key, value in d.items() if value == n]
# print(min(m))


# Исправление дубликатов 🌶️
# s = input().split()
# d = {}
# a = []
# for i in s:
#     d[i] = d.get(i,0) + 1
#     if d[i] == 1:
#         a += [i]
#     else:
#         a += [i + '_' + str(d[i] - 1)]
#
# print(*a)
#


# Словарь программиста
# n = int(input())
# d = {}
# for i in range(n):
#     temp = input().split(': ')
#     d[temp[0].lower()] = temp[1]
#
# m = int(input())
# a = [input().lower() for _ in range(m)]
# for i in a:
#     if i in d:
#         print(d[i])
#     else:
#         print('Не найдено')


# Анаграммы 1
# a = input()
# b = input()
#
# da = dict()
# db = {}
# for i in a:
#     da[i] = da.get(i, 0) + 1
# for i in b:
#     db[i] = db.get(i, 0) + 1
# if da == db:
#     print('YES')
# else:
#     print('NO')

# Анаграммы 2
# a = input().lower().split()
# b = input().lower().split()
# sa = ''
# sb = ''
# for i in a:
#     sa += i.strip('.,!?:;-')
# for i in b:
#     sb += i.strip('.,!?:;-')
#
# if sorted(sa) == sorted(sb):
#     print('YES')
# else:
#     print('NO')

# Словарь синонимов
# m = int(input())
# k = dict([input().split() for i in range(m)])
# a = input()
# for key, value in k.items():
#     if a == key:
#         print(value)
#     elif a == value:
#         print(key)

# # Страны и города
# d = {}
# n = int(input())
# for i in range(n):
#     a, *tail = input().split()
#     d[tuple(tail)] = a
#
# m = int(input())
# a = [input() for i in range(m)]
#
# for i in a:
#     for key in d:
#         if i in key:
#             print(d[key])

# Телефонная книга
# d = {}
# m = int(input())
# for i in range(m):
#     b = input().lower().split()
#     d[b[1]] = d.get(b[1], []) + [b[0]]
# n = int(input())
# a = tuple([input() for i in range(n)])
# for i in a:
#     j = i.lower()
#     if j in d:
#         z = d[j]
#         print(*z)
#     else:
#         print('абонент не найден')


# Секретное слово
a = input()
n = int(input())
s = [input().split(': ')[::-1] for _ in range(n)]
k = dict(s)
d = {}
for i in a:
    d[str(a.count(i))] = i

for i in a:
    for key, value in d.items():
        if i == value:
            print(k[key], end='')