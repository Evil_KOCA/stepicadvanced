# from fractions import Fraction
# import random
#
# num1 = Fraction(3, 4)     # 3 - числитель, 4 - знаменатель
# num2 = Fraction('0.55')
# num3 = Fraction('1/9')
#
# print(num1, num2, num3, sep='\n')
#
# num1 = Fraction(5, 10)
# num2 = Fraction('75/100')
# num3 = Fraction('0.25')
#
# print(num1, num2, num3, sep='\n')
#
# num1 = Fraction(1, 2)        # 1/2
# num2 = Fraction(15, 30)      # 15/30=1/2
# num3 = Fraction(3, 5)        # 3/5
# num4 = Fraction(5, 3)        # 5/3
# num5 = 1
# num6 = 0.8
#
# print(num1 == num2)
# print(num1 != num4)
# print(num2 > num3)
# print(num4 <= num1)
# print(num1 < num5)
# print(num6 > num4)
#
# num1 = Fraction('1/10')
# num2 = Fraction('2/3')
#
# print(num1 + num2)
# print(num1 - num2)
# print(num1 * num2)
# print(num1 / num2)
#
#
# num1 = Fraction('3/8')
# num2 = Fraction('1/2')
#
# print(num1 ** num2)
#
#
# from math import *
#
# num1 = Fraction('1.44')
# num2 = Fraction('0.523')
#
# print(sqrt(num1))
# print(sin(num2))
# print(log(num1 + num2))
#
#
# num = Fraction('5/16')
#
# print('Числитель дроби равен:', num.numerator)
# print('Знаменатель дроби равен:', num.denominator)
#
# num = Fraction('-5/16')
#
# print(num.as_integer_ratio())
#
#
# import math
# print('PI =', math.pi)
#
# num = Fraction(str(math.pi))
#
# print('No limit =', num)
#
# for d in [1, 5,  50, 90, 100, 500, 1000000]:
#     limited = num.limit_denominator(d)
#     print(limited)
#

from fractions import Fraction as F
import math

# numbers = ['6.34', '4.08', '3.04', '7.49', '4.45', '5.39', '7.82', '2.76', '0.71', '1.97', '2.54', '3.67', '0.14', '4.29', '1.84', '4.07', '7.26', '9.37', '8.11', '4.30', '7.16', '2.46', '1.27', '0.29', '5.12', '4.02', '6.95', '1.62', '2.26', '0.45', '6.91', '7.39', '0.52', '1.88', '8.38', '0.75', '0.32', '4.81', '3.31', '4.63', '7.84', '2.25', '1.10', '3.35', '2.05', '7.87', '2.40', '1.20', '2.58', '2.46']
#
# for i in numbers:
#     print(i, '=', F(i))

# s = '0.78 4.3 9.6 3.88 7.08 5.88 0.23 4.65 2.79 0.90 4.23 2.15 3.24 8.57 0.10 8.57 1.49 5.64 3.63 8.36 1.56 6.67 1.46 5.26 4.83 7.13 1.22 1.02 7.82 9.97 5.40 9.79 9.82 2.78 2.96 0.07 1.72 7.24 7.84 9.23 1.71 6.24 5.78 5.37 0.03 9.60 8.86 2.73 5.83 6.50 0.123 0.00021'
#
# a = [F(i) for i in s.split()]
# t = min(a) + max(a)
# print(t)

# n, m = input(), input()
# print(n, '+', m, '=', F(n) + F(m))
# print(n, '-', m, '=', F(n) - F(m))
# print(n, '*', m, '=', F(n) * F(m))
# print(n, '/', m, '=', F(n) / F(m))


# n = int(input())
# s = 0
# for i in range(1, n + 1):
#     a = F(1, i ** 2)
#     s += a
# print(s)

# n = int(input())
# s = 0
# for i in range(1, n + 1):
#     a = F(1, math.factorial(i))
#     s += a
# print(s)
# print('Числитель дроби равен:', num.numerator)
# print('Знаменатель дроби равен:', num.denominator)

# Юный математик 🌶️
# n = int(input())
# q = []
# for i in range(1, n ):
#     a = i
#     b = n - i
#     s = F(a, b)
#     if a < b and s.numerator == a and s.denominator == b:
#         q.append(s)
# print(max(q))


# Упорядоченные дроби
# n = int(input())
# q = []
# for i in range(1, n + 1):
#     for j in range(1, i + 1):
#         a = j
#         b = i
#         s = F(a, b)
#         if a < b and s.numerator == a and s.denominator == b:
#             q.append(s)
# q = sorted(q, reverse=False)
# for i in q:
#     print(i)


import cmath
# a, b = complex(input()), complex(input())
# print(a, '+', b , '=', a + b)
# print(a, '-', b , '=', a - b)
# print(a, '*', b , '=', a * b)


# numbers = [3 + 4j, 3 + 1j, -7 + 3j, 4 + 8j, -8 + 10j, -3 + 2j, 3 - 2j, -9 + 9j, -1 - 1j, -1 - 10j, -20 + 15j, -21 + 1j, 1j, -3 + 8j, 4 - 6j, 8 + 2j, 2 + 3j]
# s = {key: abs(key) for key in numbers}
# for key, value in s.items():
#     if value == max(s.values()):
#         b = max(s.values())
#         a = key
# print(a)
# print(b)

n = int(input())
a, b = complex(input()), complex(input())

q = a ** n + a.conjugate() ** n + b ** n + b.conjugate() ** (n + 1)
print(q)
