import random
import string

# # Модуль random
# # Функция randint()
# num1 = random.randint(0, 17)
# num2 = random.randint(-5, 5)
#
# for _ in range(10):
#     print(random.randint(1, 100))
#
# # Функция randrange()
# num = random.randrange(10)
# num = random.randrange(5, 10)
# num = random.randrange(0, 101, 10)
#
# # Функция random()
# num = random.random()
# print(num)
#
# # Функция uniform()
# num = random.uniform(1.5, 17.3)
# print(num)
#
# # Функция seed()
# random.seed(17)   # явно устанавливаем начальное значение для генератора случайных чисел
#
# for _ in range(10):
#     print(random.randint(1, 100))

# Монетка
# n = int(input())
# for _ in range(n):
#     a = random.randint(1,2)
#     if a == 1:
#         print('Орел')
#     elif a == 2:
#         print('Решка')

# pass
# n = int(input())    # количество попыток
# z = []
# s = ''
# for i in range(n):
#     a = random.randint(1, 2)
#     if a == 1:
#         num = random.randint(65, 90)
#     elif a == 2:
#         num = random.randint(97, 122)
#     z += [num]
# for i in z:
#     a = chr(i)
#     s += a
# print(s)
#
# # LOTO
# s = set()
# while len(s) < 7:
#     s.add(random.randint(1,49))
# a = sorted(s)
# print(*a)
#
# import string
#
# print(string.ascii_letters)
# print(string.ascii_uppercase)
# print(string.ascii_lowercase)
# print(string.digits)
# print(string.hexdigits)
# print(string.octdigits)
# print(string.punctuation)
# print(string.printable)
#
#
# def generate_ip():
#     s = str(random.randint(0,255)) + '.'+str(random.randint(0,255)) + '.'+str(random.randint(0,255)) + '.'+str(random.randint(0,255))
#     return s
# print(generate_ip())
#
#
#
# def generate_index():
#     s = random.choice(string.ascii_uppercase) + random.choice(string.ascii_uppercase) + str(random.randint(0, 99)) + '_' + str(random.randint(0, 99)) + random.choice(string.ascii_uppercase) + random.choice(string.ascii_uppercase)
#     return s
# print(generate_index())
#
# matrix = [[1, 2, 3, 4],
#           [5, 6, 7, 8],
#           [9, 10, 11, 12],
#           [13, 14, 15, 16]]
# random.shuffle(matrix)
# print(matrix)
#
#
# s = set()
# while len(s) < 100:
#     n = str(random.randint(1,9))
#     for i in range(6):
#         a = str(random.randint(0, 9))
#         n += a
#     s.add(n)
#
# for i in s:
#     print(i)
#
#
# a = input()
# q = list(a)
# random.shuffle(q)
# for i in q:
#     print(i, end='')


# s = set()
# while len(s) < 25:
#     n = str(random.randint(1,75))
#     s. add(n)
#
# a = [[s.pop() for j in range(5)] for i in range(5)]
# a[2][2] = 0
# for i in a:
#     for j in i:
#         print(str(j).ljust(3), end='')
#     print()




# Тайный друг 🌶️
# n = int(input())
# s = [input() for i in range(n)]
# random.shuffle(s)
#
#
#
# z = [[s[i], s [i - 1]] for i in range(n)]
# for i in z:
#     [a, b] = i
#     print(a, '-', b)

# Генератор паролей 1 #  Генератор паролей 2 🌶️
# e = 'lI1oO0'
# a = set(string.ascii_uppercase) - set(e)
# b = set(string.ascii_lowercase) - set(e)
# c = set(string.digits) - set(e)
# d = a | b | c
# n = int(input())
# m = int(input())
# for i in range(n):
#     l = random.sample(d, m-3) + random.sample(a,1) + random.sample(b,1) + random.sample(c,1)
#     for j in l:
#         print(j, end='')
#     print()

#
# # 1
# n = 1000
# k = 0
# s0 = 1
# for _ in range(n):
#     x = random.uniform(0, 1)     # случайное число с плавающей точкой от 0 до 1
#     y = random.uniform(0, 1)     # случайное число с плавающей точкой от 0 до 1
#
#     if y <= x**2:                # если попадает в нужную область
#         k += 1
#
# print((k/n)*s0)
#
#
# # 2
# n = 1000
# k = 0
# s0 = 16
# for _ in range(n):
#     x = random.uniform(-2, 2)
#     y = random.uniform(-2, 2)
#
#     if y**3 - 2*x**2 <= -1 and 2*y + x**3 <= 3:
#         k += 1
#
# print((k/n)*s0)


# n = 10 ** 6
# k = 0
# s = 4 * 4
# for i in range(n):
#     x = random.uniform(-2 , 2)
#     y = random.uniform(-2, 2)
#     if x ** 3 + y ** 4 + 2 >= 0 and 3 * x + y ** 2 -2 <= 0:
#         k += 1
# print((k/n)*s)

# Pi
n = 10 ** 6
k = 0
s = 2 * 2
for i in range(n):
    x = random.uniform(-1 , 1)
    y = random.uniform(-1, 1)
    if x ** 2 + y ** 2 - 1 <= 0:
        k += 1
print((k/n)*s)





