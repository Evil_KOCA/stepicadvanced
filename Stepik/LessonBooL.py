numbers = [-6, -8, 0, 1, 3, 8, -7, 12, 17, 24, 25, 3, 5, 1]
res = 0
for num in numbers:
    res += (num % 2 == 1) and (num > 1)
print(res)

numbers = [1, 2, 3, 4, 5, 8, 10, 12, 15, 17]
res = 0

for num in numbers:
    res += (num % 2 == 0)
print(res)


# Для приведения других типов данных к булеву существует функция bool(), работающая по следующим соглашениям:
#
# строки: пустая строка — ложь (False), непустая строка — истина (True);
# числа: нулевое число — ложь (False), ненулевое число (в том числе и меньшее нуля) — истина (True);
# списки: пустой список — ложь (False), непустой — истина (True).
print(bool('Beegeek'))
print(bool(17))
print(bool(['apple', 'cherry']))
print(bool())
print(bool(''))
print(bool(0))
print(bool([]))

#    В программировании функция, которая возвращает значение True или False называется предикатом.
def is_even(num):
    return num % 2 == 0
print(is_even(8))
print(is_even(7))

# В языке Python имеется встроенная функция isinstance() для проверки соответствия типа объекта какому-либо типу данных.
print(isinstance(3, int))
print(isinstance(3.5, float))
print(isinstance('Beegeek', str))
print(isinstance([1, 2, 3], list))
print(isinstance(True, bool))
print(isinstance(3.5, int))
print(isinstance('Beegeek', float))

# В языке Python имеется встроенная функция type(), позволяющая получить тип указанного в качестве аргумента объекта.
print(type(3))
print(type(3.5))
print(type('Beegeek'))
print(type([1, 2, 3]))
print(type(True))

# Функция type() часто бывает полезна при отладке программного кода, а также в реальном коде, особенно
# в объектно-ориентированном программировании с наследованием и пользовательскими строковыми представлениями, но об этом позже.
#
# Обратите внимание, что при проверке типов обычно вместо функции type() используется функцияisinstance()
# так как, она принимает во внимание иерархию типов (ООП).


# объявление функции
def func(num1, num2):
    return bool(num1 % num2)

# считываем данные
num1, num2 = int(input()), int(input())

# вызываем функцию
if func(num1, num2):
    print('не делится')
else:
    print('делится')