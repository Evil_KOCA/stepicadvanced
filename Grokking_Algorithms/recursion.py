# def countdown(i):
#     print(i)
#     if i <= 0:
#         return
#     else:
#         countdown(i - 1)
#
#
# countdown(10)
#
#
# def fact(x):
#     if x == 1:
#         return 1
#     else:
#         return x * fact(x - 1)


# def rec_summ(arr):
#     if len(arr) == 0:
#         return 0
#     else:
#         return arr[0] + rec_summ(arr[1:])
#
# arr_main = [i for i in range(20)]
# print(arr_main)
# result = rec_summ(arr_main)
# print(result)
# print(sum(arr_main))


# def len_rec(arr):
#     if not arr:
#         return 0
#     else:
#         return 1 + len_rec(arr[1:])
#
#
# arr_main = [i for i in range(20)]
# print(arr_main)
# result = len_rec(arr_main)
# print(result)
# print(len(arr_main))


def quicksort(array):
    if len(array) < 2:  # базовый случай: массивы с О и 1 эпементом уже "отсортированы"
        return array
    else:
        pivot = array[0]  # Рекурсивный Случай
        less = [i for i in array[1:] if i <= pivot]  # Подмассив всех элементов меньших опорного
        greater = [i for i in array[1:] if i > pivot]  # Подмассив всех элементов больших опорного
        return quicksort(less) + [pivot] + quicksort(greater)


print(quicksort([10, 5, 2, 3]))


