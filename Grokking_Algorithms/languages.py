get_languages_prod = [
    {
      "id": "it",
      "locale": "it-IT",
      "title": "Italian",
      "isEnabled": True,
      "createdAt": "25.09.2017 10:25:47",
      "updatedAt": "25.09.2017 10:25:47"
    },
    {
      "id": "ar",
      "locale": "ar-AE",
      "title": "Arabic",
      "isEnabled": True,
      "createdAt": "25.09.2017 10:24:21",
      "updatedAt": "25.09.2017 10:24:21"
    },
    {
      "id": "es",
      "locale": "es-ES",
      "title": "Spanish",
      "isEnabled": True,
      "createdAt": "25.09.2017 10:24:30",
      "updatedAt": "25.09.2017 10:24:30"
    },
    {
      "id": "id",
      "locale": "id-ID",
      "title": "Indonesian",
      "isEnabled": True,
      "createdAt": "25.09.2017 10:24:34",
      "updatedAt": "25.09.2017 10:24:34"
    },
    {
      "id": "ja",
      "locale": "ja-JP",
      "title": "Japanese",
      "isEnabled": True,
      "createdAt": "25.09.2017 10:24:39",
      "updatedAt": "25.09.2017 10:24:39"
    },
    {
      "id": "ko",
      "locale": "ko-KR",
      "title": "Korean",
      "isEnabled": True,
      "createdAt": "25.09.2017 10:24:43",
      "updatedAt": "25.09.2017 10:24:43"
    },
    {
      "id": "ms",
      "locale": "ms-MY",
      "title": "Malay",
      "isEnabled": True,
      "createdAt": "25.09.2017 10:24:56",
      "updatedAt": "25.09.2017 10:24:56"
    },
    {
      "id": "my",
      "locale": "my-MM",
      "title": "Burmese",
      "isEnabled": True,
      "createdAt": "25.09.2017 10:25:01",
      "updatedAt": "25.09.2017 10:25:01"
    },
    {
      "id": "pt",
      "locale": "pt-PT",
      "title": "Portuguese",
      "isEnabled": True,
      "createdAt": "25.09.2017 10:25:06",
      "updatedAt": "25.09.2017 10:25:06"
    },
    {
      "id": "th",
      "locale": "th-TH",
      "title": "Thai",
      "isEnabled": True,
      "createdAt": "25.09.2017 10:25:20",
      "updatedAt": "25.09.2017 10:25:20"
    },
    {
      "id": "tr",
      "locale": "tr-TR",
      "title": "Turkish",
      "isEnabled": True,
      "createdAt": "25.09.2017 10:25:25",
      "updatedAt": "25.09.2017 10:25:25"
    },
    {
      "id": "ur",
      "locale": "ur-PK",
      "title": "Urdu",
      "isEnabled": True,
      "createdAt": "25.09.2017 10:25:29",
      "updatedAt": "25.09.2017 10:25:29"
    },
    {
      "id": "vi",
      "locale": "vi-VN",
      "title": "Vietnamese",
      "isEnabled": True,
      "createdAt": "25.09.2017 10:25:34",
      "updatedAt": "25.09.2017 10:25:34"
    },
    {
      "id": "zh",
      "locale": "zh-CN",
      "title": "Chinese",
      "isEnabled": True,
      "createdAt": "25.09.2017 10:25:38",
      "updatedAt": "25.09.2017 10:25:38"
    },
    {
      "id": "fr",
      "locale": "fr-FR",
      "title": "French",
      "isEnabled": True,
      "createdAt": "25.09.2017 10:25:43",
      "updatedAt": "25.09.2017 10:25:43"
    },
    {
      "id": "de",
      "locale": "de-DE",
      "title": "Germany",
      "isEnabled": True,
      "createdAt": "01.01.0001 01:39:49",
      "updatedAt": "25.09.2017 11:50:04"
    },
    {
      "id": "hi",
      "locale": "hi-IN",
      "title": "Hindi",
      "isEnabled": True,
      "createdAt": "07.11.2019 11:33:27",
      "updatedAt": "07.11.2019 11:33:27"
    },
    {
      "id": "tl",
      "locale": "tl-PH",
      "title": "Filipino",
      "isEnabled": True,
      "createdAt": "03.08.2020 13:25:25",
      "updatedAt": "03.08.2020 13:25:25"
    }
  ]


get_languages_test = [
        {
            "createdAt": "25.09.2017 07:25:01",
            "id": "my",
            "isEnabled": True,
            "locale": "my-MM",
            "title": "Burmese",
            "updatedAt": "25.09.2017 07:25:01"
        },
        {
            "createdAt": "25.09.2017 07:25:06",
            "id": "pt",
            "isEnabled": True,
            "locale": "pt-PT",
            "title": "Portuguese",
            "updatedAt": "25.09.2017 07:25:06"
        },
        {
            "createdAt": "25.09.2017 07:24:21",
            "id": "ar",
            "isEnabled": True,
            "locale": "ar-AE",
            "title": "Arabic",
            "updatedAt": "25.09.2017 07:24:21"
        },
        {
            "createdAt": "25.09.2017 07:24:34",
            "id": "id",
            "isEnabled": True,
            "locale": "id-ID",
            "title": "Indonesian",
            "updatedAt": "25.09.2017 07:24:34"
        },
        {
            "createdAt": "25.09.2017 07:24:39",
            "id": "ja",
            "isEnabled": True,
            "locale": "ja-JP",
            "title": "Japanese",
            "updatedAt": "25.09.2017 07:24:39"
        },
        {
            "createdAt": "25.09.2017 07:24:43",
            "id": "ko",
            "isEnabled": True,
            "locale": "ko-KR",
            "title": "Korean",
            "updatedAt": "25.09.2017 07:24:43"
        },
        {
            "createdAt": "25.09.2017 07:24:56",
            "id": "ms",
            "isEnabled": True,
            "locale": "ms-MY",
            "title": "Malay",
            "updatedAt": "25.09.2017 07:24:56"
        },
        {
            "createdAt": "25.09.2017 07:25:20",
            "id": "th",
            "isEnabled": True,
            "locale": "th-TH",
            "title": "Thai",
            "updatedAt": "25.09.2017 07:25:20"
        },
        {
            "createdAt": "25.09.2017 07:25:25",
            "id": "tr",
            "isEnabled": True,
            "locale": "tr-TR",
            "title": "Turkish",
            "updatedAt": "25.09.2017 07:25:25"
        },
        {
            "createdAt": "25.09.2017 07:25:29",
            "id": "ur",
            "isEnabled": True,
            "locale": "ur-PK",
            "title": "Urdu",
            "updatedAt": "25.09.2017 07:25:29"
        },
        {
            "createdAt": "25.09.2017 07:25:34",
            "id": "vi",
            "isEnabled": True,
            "locale": "vi-VN",
            "title": "Vietnamese",
            "updatedAt": "25.09.2017 07:25:34"
        },
        {
            "createdAt": "25.09.2017 07:25:38",
            "id": "zh",
            "isEnabled": True,
            "locale": "zh-CN",
            "title": "Chinese",
            "updatedAt": "25.09.2017 07:25:38"
        },
        {
            "createdAt": "25.09.2017 07:25:43",
            "id": "fr",
            "isEnabled": True,
            "locale": "fr-FR",
            "title": "French",
            "updatedAt": "25.09.2017 07:25:43"
        },
        {
            "createdAt": "01.01.0001 00:00:00",
            "id": "de",
            "isEnabled": True,
            "locale": "de-DE",
            "title": "Germany",
            "updatedAt": "25.09.2017 08:50:04"
        },
        {
            "createdAt": "07.11.2019 09:33:27",
            "id": "hi",
            "isEnabled": True,
            "locale": "hi-IN",
            "title": "Hindi",
            "updatedAt": "07.11.2019 09:33:27"
        },
        {
            "createdAt": "03.08.2020 10:25:25",
            "id": "tl",
            "isEnabled": True,
            "locale": "tl-PH",
            "title": "Filipino",
            "updatedAt": "03.08.2020 10:25:25"
        },
        {
            "createdAt": "21.07.2020 10:24:43",
            "id": "es",
            "isEnabled": True,
            "locale": "esa",
            "title": "esa",
            "updatedAt": "21.07.2020 10:24:43"
        },
        {
            "createdAt": "21.07.2020 10:20:22",
            "id": "it",
            "isEnabled": True,
            "locale": "ita",
            "title": "ita",
            "updatedAt": "21.07.2020 10:20:22"
        },
        {
            "createdAt": "21.07.2020 10:19:09",
            "id": "en",
            "isEnabled": True,
            "locale": "new-En",
            "title": "en",
            "updatedAt": "21.07.2020 10:19:09"
        }
    ]

result = []

for language_json_prod in get_languages_prod:
    for language_json_test in get_languages_test:
        if not (language_json_prod["id"] == language_json_test["id"]) & (language_json_prod["locale"] == language_json_test["locale"]) & (language_json_prod["title"] == language_json_test["title"]):
            result.append(language_json_prod)

print(result)