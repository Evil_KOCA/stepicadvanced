# return masked string
def maskify(cc):
    if len(cc) <= 4:
        return cc
    else:
        return '#' * (len(cc) - 4) + cc[-4:]


def digital_root(n):
    def sum_digits(num: int):
        sum_num = sum([int(counter) for counter in str(num)])
        return sum_num

    root = n
    while root > 9:
        root = sum_digits(root)

    return root


def duplicate_encode(word: str):
    """a new string where each character in the new string is '('
        if that character appears only once in the original word, or ')'
        if that character appears more than once in the original word.
        Ignores capitalization when determining if a character is a duplicate. """

    result = ""
    for letter in word.lower():
        result += '(' if word.lower().count(letter) == 1 else ')'
    return result


def duplicate_count(text):
    return sum([1 if text.lower().count(letter) > 1 else 0 for letter in set(text.lower())])


def printer_error(text):
    return f"{sum([0 if 'abcdefghijklm'.count(letter) else 1 for letter in text])}/{len(text)}"


def is_valid_walk(walk: list):
    grid = {'n': [0, 1], 's': [0, -1], 'w': [-1, 0], 'e': [1, 0]}
    result = [0, 0]
    for direction in walk:
        result = [result[0] + grid[direction][0], result[1] + grid[direction][1]]
    return result == [0, 0] and len(walk) == 10


def song_decoder(song: str):
    return ' '.join(' '.join(song.split('WUB')).strip().split())


def persistence(numb):
    from functools import reduce
    counter = 0
    if numb < 10:
        return 0
    else:
        while numb >= 10:
            counter += 1
            numb = reduce(lambda x, y: x * y, map(int, str(numb)))

    return counter


def find_missing_letter(chars):
    alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    return list(set(alphabet[alphabet.index(chars[0]):alphabet.index(chars[-1]) + 1]) - set(chars))[0]


def rgb(r, g, b):
    def round_to_255(num: int):
        if num > 255:
            return 255
        elif num < 0:
            return 0
        else:
            return num

    def double_trouble(num: int):
        if len(format(num, 'X')) == 1:
            return '0' + format(num, 'X')
        elif len(format(num, 'X')) == 2:
            return format(num, 'X')

    return double_trouble(round_to_255(r)) + double_trouble(round_to_255(g)) + double_trouble(round_to_255(b))


def rgb1(r, g, b):
    round = lambda x: min(255, max(x, 0))
    return ("{:02X}" * 3).format(round(r), round(g), round(b))


def array_diff(a: list, b: list):
    result = [i for i in a]
    check_set = set(a).intersection(set(b))
    for element in check_set:
        inter = a.count(element)
        for counter in range(inter):
            result.remove(element)

    return result


def make_readable(seconds):
    return "{:02}:{:02}:{:02}".format(seconds // 3600, seconds % 3600 // 60, seconds % 60)


def order_weight(strng):
    pass
